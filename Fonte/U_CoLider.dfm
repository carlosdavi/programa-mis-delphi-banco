object F_Cadastro_Co_Lider: TF_Cadastro_Co_Lider
  Left = 71
  Top = 154
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Cadastro de Co-Lider'
  ClientHeight = 533
  ClientWidth = 1262
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 16
  object DBGrid1: TDBGrid
    Left = 0
    Top = 376
    Width = 1262
    Height = 157
    Align = alBottom
    DataSource = F_Dados.D_Discipulos
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1262
    Height = 41
    Align = alTop
    Caption = 'Cadastro de Co-Lider'
    Color = clHotLight
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -32
    Font.Name = 'Rockwell Condensed'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 41
    Width = 1262
    Height = 40
    ButtonHeight = 31
    Caption = 'ToolBar1'
    TabOrder = 2
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 9
      Style = tbsSeparator
    end
    object BitBtn6: TBitBtn
      Left = 8
      Top = 0
      Width = 145
      Height = 31
      Action = DataSetInsert1
      Caption = 'Tornar Co-Lider'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFDCF4D478D35A4FC32837B80C35B70949BF2171CE52DAF3D2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7CD65E3FC11437C70834CB0133
        CC0033CC0033CB0133C30230B60470CD51FFFFFFFFFFFFFFFFFFFFFFFFFAFDF9
        5ACC353DC71033CC0033CC0033CC0033CC0033CC0033CC0033CC0033CC0031BE
        0245BC1DF8FCF7FFFFFFFFFFFF81DA6440C91333CC0033CC0033CC0033CC0033
        CC0025950025950033CC0033CC0033CC0031BE0270CD51FFFFFFDBF5D34CCB22
        33CC0033CC0033CC0033CC0033CC00FFFFFFFFFFFF25950033CC0033CC0033CC
        0033CC0030B604D4F0CB7CDB5D3ECC0F33CC0033CC0033CC0033CC0033CC00FF
        FFFFFFFFFF25950033CC0033CC0033CC0033CC0033C30268CC4768D54335CC03
        33CC0033CC0033CC00259500259500FFFFFFFFFFFF2595002595002595002595
        0033CC0033CB0148BD2059D23133CC0033CC0033CC00FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF25950033CC0033CC0035B7095CD43433CC00
        33CC0033CC00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF33CC
        0033CC0033CC0037B80C6ED94A36CC0333CC0033CC0033CC0033CC0033CC00FF
        FFFFFFFFFF25950033CC0033CC0033CC0033CC0034CB014EC22884E06543CF14
        33CC0033CC0033CC0033CC0033CC00FFFFFFFFFFFF25950033CC0033CC0033CC
        0033CC0037C7086FD14EDDF6D55BD63233CC0033CC0033CC0033CC0033CC00FF
        FFFFFFFFFF33CC0033CC0033CC0033CC0033CC003FC114D7F2CEFFFFFF8DE270
        4DD22033CC0033CC0033CC0033CC0033CC0033CC0033CC0033CC0033CC0033CC
        003DC7107CD65EFFFFFFFFFFFFFBFEFB6FDB4A4DD22033CC0033CC0033CC0033
        CC0033CC0033CC0033CC0033CC0040C9135ACC35FAFDF9FFFFFFFFFFFFFFFFFF
        FFFFFF8DE2705BD63243CF1436CC0333CC0033CC0035CC033ECC0F4CCB2281DA
        64FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0F7D88CE2706CD9485C
        D43459D23166D54186DD68DEF6D6FFFFFFFFFFFFFFFFFFFFFFFF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object ToolButton2: TToolButton
      Left = 153
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object BitBtn11: TBitBtn
      Left = 161
      Top = 0
      Width = 133
      Height = 31
      Caption = 'Atualizar Tabela'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFD5E3BE7DAA3A9C892F92700D92700DA38732BDA869F0EBDCFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFB6CAE2E279E00279E003C96028C
        740C92700D92700D92700D92700DBDA869FFFFFFFFFFFFFFFFFFFFFFFFFBFBFB
        6CA01A2FBB0031C40033CC0033CC004AB9039A8A0E97750E92700D92700D9270
        0DA38732FFFFFFFFFFFFFFFFFFD5BC75A09B0E33CC0033CC0033CC0066B606A1
        9C0EBB9111BB9111A9830F92700D92700D92700DBDA869FFFFFFF7F2E2BD9211
        BB911144C50266B606B29510BB9111BB9111BB9111BB9111BB9111B48B109270
        0D92700D92700DF0EBDCDBBF6BBB9111B39410BB9111BB9111BB9111BB9111BB
        9111BB9111BB9111BB91116FB207749B0992700D807B0B8CA63FD3AC3FB29510
        44C5024DC103B29510BB9111BB9111BB9111BB9111BB9111BB911199A00D33CC
        0029A400279E0040A20ACC9C13BE9311B29510C3A112E5AC15EBB016E5AC15CA
        9A13BB9111BB9111BB911188A70B33CC002FBB00279E00279E006FC807C19512
        83BF0A47EF02B8E50FEBB016EBB016EBB016CA9A13BB9111BB91116EB20733CC
        0031C400279E00279E0054D00C35D70039EB003AF0003AF0007FE908EBB016EB
        B016E5AC15BB9111BB9111BB911199A00D44C502279E0040A20A74CE2936D900
        3AF2003AF0003AF0007FE908EBB016EBB016EBB016BB9111BB911166B60644C5
        0233CC00279E0063B023D2EBB636D9003AF0003AF0003AF000E6C915EBB0165D
        ED04B2DD0FBB9111AA980F33CC0033CC0031C40028A300D6E4BDFFFFFF80CF35
        38E1003BF40052F003E8BD15EBB0163AF00097BA0CBB911180AB0A33CC0033CC
        0031C20072B92EFFFFFFFFFFFFF2FCEF54D00C38E1003AF00077EF06E8AE16CC
        9C12BB9111A29C0E3DCB0134D10035D7004ABA0BF0F0F0FFFFFFFFFFFFFFFFFF
        F2FCEFB5CC5AC5A312CC9C13CC9C13CC9C13A4BA0E40D60136D90036D9007CCA
        33F0F0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1E1DEC479D3AC3F99
        BD0D36D90054D10C80D034DFEDC5FFFFFFFFFFFFFFFFFFFFFFFF}
      TabOrder = 2
      OnClick = BitBtn11Click
    end
    object ToolButton10: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BitBtn9: TBitBtn
      Left = 302
      Top = 0
      Width = 75
      Height = 31
      Hint = 'Fecha a Janela'
      Caption = 'Fechar'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFD7DDF4647AD43451C5193ABA1535B82847C0566ECED3D9F2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6B81D82746C40F33C6022AC800
        28C90028C90129C8062BC2092CB7536BCDFFFFFFFFFFFFFFFFFFFFFFFFFBFCFD
        4C66CF1E40C80028C90028C90026BC0021A2001E95001E950021A20026BC062B
        BD2040BCF7F8FCFFFFFFFFFFFF768BDD2445CB0028C90028C93F59C3BFC7E4FF
        FFFFFFFFFFBFC7E43F56AF001E950023AF062BBD536BCDFFFFFFDADFF6415DCF
        0028C90028C97F92DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F8ECA001E
        950026BC092CB7CCD3F07489DE1E40CD0028C93F5DD6FFFFFFFFFFFF7084D410
        35CC1035CC7086E1FFFFFFFFFFFF3F56AF0021A2062BC24B64CC687FDB052CCA
        0028C9BFC9F1FFFFFF6F80C30027C60028C90028C90028C96F86E1FFFFFFBFC7
        E4001E950129C82847BE5D76D90028C90028C9FFFFFFFFFFFF102C9C0027C600
        28C90027C60027C61035CCFFFFFFFFFFFF001E950028C91535B8627ADB0028C9
        0028C9FFFFFFFFFFFF0F2B9B0024B20F35CC0F2CA2001F9C0F35CCFFFFFFFFFF
        FF0021A50028C9193ABA748AE1062DCA0028C9AFBCEEFFFFFF6F80C3001E95DF
        E4F8DFE3F2001E956F86E1FFFFFFAFB8DE0025B9022AC83552C48195E42748D1
        0028C9506BDAFFFFFFFFFFFF001E95FFFFFFFFFFFF001E95FFFFFFFFFFFF5067
        C40028C90F33C65971D2DEE4F85E77DD0028C90028C99FAEEBFFFFFF0025B9FF
        FFFFFFFFFF001F9CFFFFFF9FADE50028C90028C92746C4D1D8F2FFFFFF8C9FE7
        3C5BD60028C90028C9506BDA0028C9DFE4F8DFE4F80027C6506BDA0028C90028
        C91E40C86B81D8FFFFFFFFFFFFFEFEFF7489E23C5BD60028C90028C90028C910
        35CC1035CC0028C90028C90028C92445CB4C66CFFAFBFDFFFFFFFFFFFFFFFFFF
        FFFFFF8C9FE75E77DD2748D1062DCA0028C90028C9052CCA1E40CD415DCF768B
        DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFE4F88C9EE66F85E062
        7ADB5D76D9637BDB7F92E0DBE0F7FFFFFFFFFFFFFFFFFFFFFFFF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BitBtn9Click
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 96
    Width = 1007
    Height = 257
    Caption = 'Pesquisa de Membro'
    TabOrder = 3
    object DBText1: TDBText
      Left = 144
      Top = 144
      Width = 65
      Height = 17
      DataField = 'codpastor'
      DataSource = F_Dados.D_Pastores
    end
    object Label1: TLabel
      Left = 24
      Top = 144
      Width = 105
      Height = 16
      Caption = 'C'#243'digo do Pastor'
    end
    object Label2: TLabel
      Left = 40
      Top = 172
      Width = 165
      Height = 16
      Caption = 'Nome do NOVO Co-L'#237'der: '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 64
      Top = 192
      Width = 104
      Height = 16
      Caption = 'C'#243'digo da Base:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 208
      Top = 176
      Width = 433
      Height = 17
      DataField = 'nome_discipulo'
      DataSource = F_Dados.D_Discipulos
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText3: TDBText
      Left = 208
      Top = 192
      Width = 385
      Height = 17
      DataField = 'codbase'
      DataSource = F_Dados.D_Discipulos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 48
      Top = 208
      Width = 135
      Height = 16
      Caption = 'C'#243'digo do Disc'#237'pulo: '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText4: TDBText
      Left = 208
      Top = 208
      Width = 385
      Height = 17
      DataField = 'coddiscipulo'
      DataSource = F_Dados.D_Discipulos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 192
      Top = 48
      Width = 393
      Height = 31
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn12: TBitBtn
      Left = 600
      Top = 48
      Width = 81
      Height = 33
      Caption = 'Pesquisar'
      TabOrder = 1
      OnClick = BitBtn12Click
    end
    object RadioGroup1: TRadioGroup
      Left = 32
      Top = 24
      Width = 137
      Height = 65
      Caption = 'Pesquisa Por:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'Nome'
        'C'#243'digo')
      ParentFont = False
      TabOrder = 2
    end
    object GroupBox2: TGroupBox
      Left = 2
      Top = 104
      Width = 1003
      Height = 9
      TabOrder = 3
    end
  end
  object ActionList1: TActionList
    Left = 1040
    Top = 88
    object DataSetInsert1: TDataSetInsert
      Category = 'Dataset'
      Caption = 'Inserir'
      OnExecute = DataSetInsert1Execute
    end
    object DataSetPost1: TDataSetPost
      Category = 'Dataset'
      Caption = 'Salvar'
      OnExecute = DataSetPost1Execute
    end
    object DataSetCancel1: TDataSetCancel
      Category = 'Dataset'
      Caption = 'Cancelar'
      OnExecute = DataSetCancel1Execute
    end
    object DataSetEdit1: TDataSetEdit
      Category = 'Dataset'
      Caption = 'Alterar'
      OnExecute = DataSetEdit1Execute
    end
    object DataSetDelete1: TDataSetDelete
      Category = 'Dataset'
      Caption = 'Excluir'
      OnExecute = DataSetDelete1Execute
    end
    object DataSetFirst1: TDataSetFirst
      Category = 'Dataset'
      Caption = 'Primeiro Registro'
      OnExecute = DataSetFirst1Execute
    end
    object DataSetNext1: TDataSetNext
      Category = 'Dataset'
      Caption = 'Pr'#243'ximo Registro'
      OnExecute = DataSetNext1Execute
    end
    object DataSetLast1: TDataSetLast
      Category = 'Dataset'
      Caption = 'Registro Anterior'
      OnExecute = DataSetLast1Execute
    end
    object DataSetPrior1: TDataSetPrior
      Category = 'Dataset'
      Caption = 'Ultimo Registro'
      OnExecute = DataSetPrior1Execute
    end
  end
end
