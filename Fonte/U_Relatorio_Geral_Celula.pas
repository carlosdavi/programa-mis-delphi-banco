unit U_Relatorio_Geral_Celula;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.Mask, Vcl.DBCtrls;

type
  TF_Relatorio_Geral_Celula = class(TForm)
    Panel1: TPanel;
    DBGrid4: TDBGrid;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    DBGrid3: TDBGrid;
    GroupBox5: TGroupBox;
    RadioGroup1: TRadioGroup;
    DBGrid1: TDBGrid;
    Label2: TLabel;
    Label3: TLabel;
    Edit1: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Button1: TButton;
    DBGrid2: TDBGrid;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    DBText9: TDBText;
    DBText10: TDBText;
    DBText11: TDBText;
    DBText12: TDBText;
    DBText13: TDBText;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    procedure RadioGroup1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ass(Column: TColumn);
    procedure ss(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure DBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGrid5CellClick(Column: TColumn);
    procedure DBGrid5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2ColEnter(Sender: TObject);
    procedure aa(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    codDiscipulo:Integer;
    codLider:Integer;
    codBase:Integer;
    codGeracao:Integer;
    codCelula:Integer;

  end;

var
  F_Relatorio_Geral_Celula: TF_Relatorio_Geral_Celula;

implementation

{$R *.dfm}

uses U_Dados;

procedure TF_Relatorio_Geral_Celula.Button1Click(Sender: TObject);
begin
//Setando os DBGrid para evitar bug
DBGrid1.DataSource:=F_Dados.D_Discipulos;
DBGrid2.DataSource:=F_Dados.D_Para_Grids_Vazios;
DBGrid3.DataSource:=F_Dados.D_Para_Grids_Vazios;
DBGrid4.DataSource:=F_Dados.D_Para_Grids_Vazios;

//implementação de pesquisa por todos os lideres
if (RadioGroup1.ItemIndex=0)then
  Begin
  WITH F_Dados.Q_Discipulos DO
    Begin
       Close;
       SQL.Clear;
       Sql.Add('Select * From discipulos where E_Lider=1');
       open;
    End;
  End;

  //implementação de pesquisa por nome
if (RadioGroup1.ItemIndex=1)then
  Begin
  WITH F_Dados.Q_Discipulos DO
    Begin
       Close;
       SQL.Clear;
       Sql.Add('Select * From Discipulos where nome_discipulo like :n and E_Lider=1');
       Parameters[0].Value:='%'+Edit1.Text+'%';
       open;
       codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;//pegando o cod discipulo
       End;
      //agora a pesquisa do codigo do lider
      WITH F_dados.Q_Lideres DO
        Begin
            Close;
            Sql.clear;
            Sql.Add('select * from lideres where coddiscipulo=:c');
            Parameters[0].Value:=codDiscipulo;
            Open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
       End;

       //implementação de pesquisa por Geração
if(RadioGroup1.ItemIndex=2)Then
  Begin
       WITH F_dados.Q_Geracoes DO
          Begin
              close;
              sql.Clear;
              sql.Add('select * from geracoes where nomeGeracao like :c');
              Parameters[0].Value:='%'+Edit1.Text+'%';
              open;
              codGeracao:=F_dados.Q_Geracoescodgeracao.Value; //passando codGeracao
          End;
      WITH F_dados.Q_Discipulos DO
          Begin
              close;
              sql.Clear;
              sql.Add('select * from discipulos where codgeracao=:c and E_Lider=1');
              Parameters[0].Value:=codGeracao;
              open;
          End;
  End;

 //implementação de pesquisa por Territorio
if(RadioGroup1.ItemIndex=3)Then
  Begin
      WITH F_dados.Q_Bases DO
      Begin
          close;
          sql.Clear;
          sql.Add('select * from Bases where nome_base like :n');
          Parameters[0].Value:='%'+Edit1.Text+'%';
          open;
          codBase:=F_dados.Q_Basescodbase.Value; //copiando codBase
      End;
      //implementação para pegar o cod discipulo
      WITH F_dados.Q_Discipulos DO
        begin
          close;
          sql.Clear;
          sql.Add('select * from Discipulos where codbase=:c and E_Lider=1');
          Parameters[0].Value:=codBase;
          Open;
        end;
  End;
//implementação de pesquisa por datas
if(RadioGroup1.ItemIndex=4)THEN
  //vamos partir do principio de que sao todos os lideres
  Begin
      //primeiro vamos selecionar todos os lideres
       WITH F_Dados.Q_Discipulos DO
    Begin
       Close;
       SQL.Clear;
       Sql.Add('Select * From discipulos where E_Lider=1');
       open;
    End;
    //A implementação continua na grid
  End;

  end;//fim procedure

procedure TF_Relatorio_Geral_Celula.DBGrid1CellClick(Column: TColumn);
begin
//foi posto esse dbgid aqui para evitar bug
DBGrid2.DataSource:=F_Dados.D_Celulas;

    //Agora a pesquisa na Grid de celulas
if(RadioGroup1.ItemIndex=0)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;

          End;
        //Agora pesquisa a celula do lider
        WITH F_dados.Q_Celulas DO
          begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;
 //implementação no caso de selecionar o TERRITORIO
 if(RadioGroup1.ItemIndex=2)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from Lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
          End;
          codLider:=F_dados.Q_Liderescodlider.Value;
          //Label17.Caption:=inttoSTR(codLider);
        WITH F_dados.Q_Celulas DO
          Begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;

  //pesquisando o lider pelo codigo do discipulo
 if(RadioGroup1.ItemIndex=3)THEN
  Begin
        WITH F_dados.Q_Lideres DO
          Begin
            close;
            sql.Clear;
            sql.Add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
          //pesquisando a celula pelo codlider
        WITH F_dados.Q_Celulas DO
        Begin
            close;
            sql.Clear;
            sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End;
  //pesquisando a celula pelo cod lider
 if(RadioGroup1.ItemIndex=4)THEN
   Begin
         WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;

          End;
        //Agora pesquisa a celula do lider
        WITH F_dados.Q_Celulas DO
          begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
          //depois daqui vamos pesquisar a celula por data
   End

  Else
    Begin
       WITH F_dados.Q_Celulas DO
        Begin
            Close;
            Sql.Clear;
            Sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End;
end;

procedure TF_Relatorio_Geral_Celula.DBGrid1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//foi posto esse dbgid aqui para evitar bug
DBGrid2.DataSource:=F_Dados.D_Celulas;

    //Agora a pesquisa na Grid de celulas
if(RadioGroup1.ItemIndex=0)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
        //Agora pesquisa a celula do lider
        WITH F_dados.Q_Celulas DO
          begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;
 //implementação no caso de selecionar o TERRITORIO
 if(RadioGroup1.ItemIndex=2)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from Lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
          End;
          codLider:=F_dados.Q_Liderescodlider.Value;
          //Label17.Caption:=inttoSTR(codLider);
        WITH F_dados.Q_Celulas DO
          Begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;

  //pesquisando o lider pelo codigo do discipulo
 if(RadioGroup1.ItemIndex=3)THEN
  Begin
        WITH F_dados.Q_Lideres DO
          Begin
            close;
            sql.Clear;
            sql.Add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
          //pesquisando a celula pelo codlider
        WITH F_dados.Q_Celulas DO
        Begin
            close;
            sql.Clear;
            sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End;
 //pesquisando a celula pelo cod lider
 if(RadioGroup1.ItemIndex=4)THEN
   Begin
         WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;

          End;
        //Agora pesquisa a celula do lider
        WITH F_dados.Q_Celulas DO
          begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
          //depois daqui vamos pesquisar a celula por data
   End

  Else
    Begin
       WITH F_dados.Q_Celulas DO
        Begin
            Close;
            Sql.Clear;
            Sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End;
end;

procedure TF_Relatorio_Geral_Celula.ass(Column: TColumn);
begin
//foi posto esse dbgid aqui para evitar bug
DBGrid2.DataSource:=F_Dados.D_Celulas;

    //Agora a pesquisa na Grid de celulas
if(RadioGroup1.ItemIndex=0)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
        //Agora pesquisa a celula do lider
        WITH F_dados.Q_Celulas DO
          begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;
 //implementação no caso de selecionar o TERRITORIO
 if(RadioGroup1.ItemIndex=2)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from Lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
          End;
          codLider:=F_dados.Q_Liderescodlider.Value;
          //Label17.Caption:=inttoSTR(codLider);
        WITH F_dados.Q_Celulas DO
          Begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;

  //pesquisando o lider pelo codigo do discipulo
 if(RadioGroup1.ItemIndex=3)THEN
  Begin
        WITH F_dados.Q_Lideres DO
          Begin
            close;
            sql.Clear;
            sql.Add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
          //pesquisando a celula pelo codlider
        WITH F_dados.Q_Celulas DO
        Begin
            close;
            sql.Clear;
            sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End


  Else
    Begin
       WITH F_dados.Q_Celulas DO
        Begin
            Close;
            Sql.Clear;
            Sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End;
  //end;
end;

procedure TF_Relatorio_Geral_Celula.DBGrid2CellClick(Column: TColumn);
begin
//Ao clicar em alguma celula sera executado esse codigo
//ativando a Grid
codCelula:=0;
DBGrid3.DataSource:=F_Dados.D_Rel_Celulas;



if(RadioGroup1.ItemIndex=4)THEN
  Begin
     WITH F_Dados.Q_Rel_Celulas DO
    //fazendo pesquisa para o discipulo(lider) que esta selecionado
       Begin
          close;
          sql.clear;
          sql.add('select * from relatorios_celulas where (convert(Date,dataCelula,103) between Convert(Date,:d1,103) and Convert(Date,:d2,103)) and codcelula=:c order by codRelCelula desc');
          codCelula:=F_dados.Q_Celulascodcelula.Value;
          Parameters[0].Value:=MaskEdit1.Text;
          Parameters[1].Value:=MaskEdit2.Text;
          Parameters[2].Value:=codCelula;
          open;
       End;
  End
Else
  Begin
//Aqui começa a implementação
    WITH F_Dados.Q_Rel_Celulas DO
      //fazendo pesquisa para o discipulo(lider) que esta selecionado
        Begin
           close;
           sql.clear;
           sql.add('select * from relatorios_celulas where codcelula=:c order by codRelCelula desc');
           codCelula:=F_dados.Q_Celulascodcelula.Value;
           Parameters[0].Value:=codCelula;
           open;
        End;
   End;

//pesquisa de discipulos da celula
WITH F_dados.Q_Visao_DiscipuloPorCelula DO
  Begin
    close;
    sql.Clear;
    sql.Add('select * from visao_DiscipulosPorCelula where codcelula=:c');
    Parameters[0].Value:=F_dados.Q_Celulascodcelula.Value;
    open;
    DBGrid4.DataSource:=F_dados.D_Visao_DiscipuloPorCelula;
  End;
end;
procedure TF_Relatorio_Geral_Celula.DBGrid2ColEnter(Sender: TObject);
begin
//Ao clicar em alguma celula sera executado esse codigo
//ativando a Grid
codCelula:=0;
DBGrid3.DataSource:=F_Dados.D_Rel_Celulas;



if(RadioGroup1.ItemIndex=4)THEN
  Begin
     WITH F_Dados.Q_Rel_Celulas DO
    //fazendo pesquisa para o discipulo(lider) que esta selecionado
       Begin
          close;
          sql.clear;
          sql.add('select * from relatorios_celulas where (convert(Date,dataCelula,103) between Convert(Date,:d1,103) and Convert(Date,:d2,103)) and codcelula=:c order by codRelCelula desc');
          codCelula:=F_dados.Q_Celulascodcelula.Value;
          Parameters[0].Value:=MaskEdit1.Text;
          Parameters[1].Value:=MaskEdit2.Text;
          Parameters[2].Value:=codCelula;
          open;
       End;
  End

Else
  Begin
//Aqui começa a implementação
    WITH F_Dados.Q_Rel_Celulas DO
      //fazendo pesquisa para o discipulo(lider) que esta selecionado
        Begin
           close;
           sql.clear;
           sql.add('select * from relatorios_celulas where codcelula=:c order by codRelCelula desc');
           codCelula:=F_dados.Q_Celulascodcelula.Value;
           Parameters[0].Value:=codCelula;
           open;
        End;
   End;
//pesquisa de discipulos da celula
WITH F_dados.Q_Visao_DiscipuloPorCelula DO
  Begin
    close;
    sql.Clear;
    sql.Add('select * from visao_DiscipulosPorCelula where codcelula=:c');
    Parameters[0].Value:=F_dados.Q_Celulascodcelula.Value;
    open;
    DBGrid4.DataSource:=F_dados.D_Visao_DiscipuloPorCelula;
  End;
end;

procedure TF_Relatorio_Geral_Celula.DBGrid2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin

end;

//fim procedure

procedure TF_Relatorio_Geral_Celula.ss(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//foi posto esse dbgid aqui para evitar bug
DBGrid2.DataSource:=F_Dados.D_Celulas;

    //Agora a pesquisa na Grid de celulas
if(RadioGroup1.ItemIndex=0)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
        //Agora pesquisa a celula do lider
        WITH F_dados.Q_Celulas DO
          begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;
 //implementação no caso de selecionar o TERRITORIO
 if(RadioGroup1.ItemIndex=2)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from Lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
          End;
          codLider:=F_dados.Q_Liderescodlider.Value;
          //Label17.Caption:=inttoSTR(codLider);
        WITH F_dados.Q_Celulas DO
          Begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;

  //pesquisando o lider pelo codigo do discipulo
 if(RadioGroup1.ItemIndex=3)THEN
  Begin
        WITH F_dados.Q_Lideres DO
          Begin
            close;
            sql.Clear;
            sql.Add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
          //pesquisando a celula pelo codlider
        WITH F_dados.Q_Celulas DO
        Begin
            close;
            sql.Clear;
            sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End


  Else
    Begin
       WITH F_dados.Q_Celulas DO
        Begin
            Close;
            Sql.Clear;
            Sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End;
  //end;
end;

procedure TF_Relatorio_Geral_Celula.DBGrid2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
///Ao clicar em alguma celula sera executado esse codigo
//ativando a Grid
codCelula:=0;
DBGrid3.DataSource:=F_Dados.D_Rel_Celulas;

if(RadioGroup1.ItemIndex=4)THEN
  Begin
     WITH F_Dados.Q_Rel_Celulas DO
    //fazendo pesquisa para o discipulo(lider) que esta selecionado
       Begin
          close;
          sql.clear;
          sql.add('select * from relatorios_celulas where (convert(Date,dataCelula,103) between Convert(Date,:d1,103) and Convert(Date,:d2,103)) and codcelula=:c order by codRelCelula desc');
          codCelula:=F_dados.Q_Celulascodcelula.Value;
          Parameters[0].Value:=MaskEdit1.Text;
          Parameters[1].Value:=MaskEdit2.Text;
          Parameters[2].Value:=codCelula;
          open;
       End;
  End
Else
  Begin
//Aqui começa a implementação
    WITH F_Dados.Q_Rel_Celulas DO
      //fazendo pesquisa para o discipulo(lider) que esta selecionado
        Begin
           close;
           sql.clear;
           sql.add('select * from relatorios_celulas where codcelula=:c order by codRelCelula desc');
           codCelula:=F_dados.Q_Celulascodcelula.Value;
           Parameters[0].Value:=codCelula;
           open;
        End;
   End;
//pesquisa de discipulos da celula
WITH F_dados.Q_Visao_DiscipuloPorCelula DO
  Begin
    close;
    sql.Clear;
    sql.Add('select * from visao_DiscipulosPorCelula where codcelula=:c');
    Parameters[0].Value:=F_dados.Q_Celulascodcelula.Value;
    open;
    DBGrid4.DataSource:=F_dados.D_Visao_DiscipuloPorCelula;
  End;
end;

procedure TF_Relatorio_Geral_Celula.DBGrid2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//foi posto esse dbgid aqui para evitar bug
DBGrid2.DataSource:=F_Dados.D_Celulas;

    //Agora a pesquisa na Grid de celulas
if(RadioGroup1.ItemIndex=0)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
        //Agora pesquisa a celula do lider
        WITH F_dados.Q_Celulas DO
          begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;
 //implementação no caso de selecionar o TERRITORIO
 if(RadioGroup1.ItemIndex=2)THEN
  Begin
       WITH F_dados.Q_Lideres DO
         //fazendo pesquisa para o discipulo(lider) que esta selecionado
         Begin
            close;
            sql.clear;
            sql.add('select * from Lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
          End;
          codLider:=F_dados.Q_Liderescodlider.Value;
          //Label17.Caption:=inttoSTR(codLider);
        WITH F_dados.Q_Celulas DO
          Begin
              close;
              sql.Clear;
              sql.Add('select * from celulas where codlider=:c');
              Parameters[0].Value:=codLider;
              open;
          End;
  End;

  //pesquisando o lider pelo codigo do discipulo
 if(RadioGroup1.ItemIndex=3)THEN
  Begin
        WITH F_dados.Q_Lideres DO
          Begin
            close;
            sql.Clear;
            sql.Add('select * from lideres where coddiscipulo=:c');
            codDiscipulo:=F_dados.Q_Discipuloscoddiscipulo.Value;
            Parameters[0].Value:=codDiscipulo;
            open;
            codLider:=F_dados.Q_Liderescodlider.Value;
          End;
          //pesquisando a celula pelo codlider
        WITH F_dados.Q_Celulas DO
        Begin
            close;
            sql.Clear;
            sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End


  Else
    Begin
       WITH F_dados.Q_Celulas DO
        Begin
            Close;
            Sql.Clear;
            Sql.Add('select * from celulas where codlider=:c');
            Parameters[0].Value:=codLider;
            open;
        End;
  End;
  //end;
end;

procedure TF_Relatorio_Geral_Celula.aa(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
WITH F_dados.Q_Discipulos DO
  Begin
    close;
    sql.Clear;
    sql.Add('select * from discipulos where codcelula=:c');
    Parameters[0].Value:=F_dados.Q_Celulascodcelula.Value;
    open;
  End;
end;

procedure TF_Relatorio_Geral_Celula.DBGrid5CellClick(Column: TColumn);
begin
//Ao clicar em alguma celula sera executado esse codigo
//ativando a Grid
F_dados.Q_Rel_Celulas.Close;
F_dados.Q_Rel_Celulas.Open;
DBGrid3.DataSource:=F_Dados.D_Rel_Celulas;

if(RadioGroup1.ItemIndex=4)THEN
  Begin
     WITH F_Dados.Q_Rel_Celulas DO
    //fazendo pesquisa para o discipulo(lider) que esta selecionado
       Begin
          close;
          sql.clear;
          sql.add('select * from relatorios_celulas where (convert(Date,dataCelula,103) between Convert(Date,:d1,103) and Convert(Date,:d2,103)) and codcelula=:c order by codRelCelula desc');
          codCelula:=F_dados.Q_Celulascodcelula.Value;
          Parameters[0].Value:=MaskEdit1.Text;
          Parameters[1].Value:=MaskEdit2.Text;
          Parameters[2].Value:=codCelula;
          open;
       End;
  End

Else
  Begin
//Aqui começa a implementação
    WITH F_Dados.Q_Rel_Celulas DO
    //fazendo pesquisa para o discipulo(lider) que esta selecionado
       Begin
          close;
          sql.clear;
          sql.add('select * from relatorios_celulas where codcelula=:c order by codRelCelula desc');
          codCelula:=F_dados.Q_Celulascodcelula.Value;
          Parameters[0].Value:=codCelula;
          open;
       End;
   End;


//pesquisa de discipulos da celula
WITH F_dados.Q_Visao_DiscipuloPorCelula DO
  Begin
    close;
    sql.Clear;
    sql.Add('select * from Visao_DiscipulosPorCelula where codcelula=:c');
    Parameters[0].Value:=F_dados.Q_Celulascodcelula.Value;
    open;
    DBGrid4.DataSource:=F_Dados.D_Visao_DiscipuloPorCelula;
  End;

end;

procedure TF_Relatorio_Geral_Celula.DBGrid5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//Ao clicar em alguma celula sera executado esse codigo
//ativando a Grid

DBGrid3.DataSource:=F_Dados.D_Rel_Celulas;

//Aqui começa a implementação
WITH F_Dados.Q_Rel_Celulas DO
  Begin
     //fazendo pesquisa para o discipulo(lider) que esta selecionado
           Begin
              close;
              sql.clear;
              sql.add('select * from relatorios_celulas where codcelula=:c order by codRelCelula desc');
              codCelula:=F_dados.Q_Celulascodcelula.Value;
              Parameters[0].Value:=codCelula;
              open;
            End;

  End;
//pesquisa de discipulos da celula
WITH F_dados.Q_Visao_DiscipuloPorCelula DO
  Begin
    close;
    sql.Clear;
    sql.Add('select * from visao_DiscipulosPorCelula where codcelula=:c');
    Parameters[0].Value:=F_dados.Q_Celulascodcelula.Value;
    open;
    DBGrid4.DataSource:=F_Dados.D_Visao_DiscipuloPorCelula;
  End;
end;

procedure TF_Relatorio_Geral_Celula.FormActivate(Sender: TObject);
begin
//desativando grid afim de evitar bugs
DBGrid1.DataSource:=F_dados.D_Para_Grids_Vazios;
DBGrid2.DataSource:=F_dados.D_Para_Grids_Vazios;
DBGrid3.DataSource:=F_dados.D_Para_Grids_Vazios;



end;

procedure TF_Relatorio_Geral_Celula.RadioGroup1Click(Sender: TObject);
begin
//Implementação na opção "Nome do Lider"
if(RadioGroup1.ItemIndex=0)THEN
  Begin
    Edit1.Visible:=true;
    Edit1.Text:='';
    MaskEdit1.Visible:=false;
    MaskEdit2.Visible:=false;
    Label5.Visible:=false;
    Label4.Caption:='Informação:';
    Label4.Left:=4;
    Button1.Left:=288;
  End;
//Implementação na opção "Geração"
if(RadioGroup1.ItemIndex=1)THEN
  Begin
    Edit1.Visible:=true;
    Edit1.Text:='';
    MaskEdit1.Visible:=false;
    MaskEdit2.Visible:=false;
    Label5.Visible:=false;
    Label4.Caption:='Informação:';
    Label4.Left:=4;
    Button1.Left:=288;
  End;
//Implementação na opção "Territorio"
if(RadioGroup1.ItemIndex=2)THEN
  Begin
    Edit1.Visible:=true;
    Edit1.Text:='';
    MaskEdit1.Visible:=false;
    MaskEdit2.Visible:=false;
    Label5.Visible:=false;
    Label4.Caption:='Informação:';
    Label4.Left:=4;
    Button1.Left:=288;

  End;
  //Implementação na opção "Territorio"
if(RadioGroup1.ItemIndex=3)THEN
  Begin
    Edit1.Visible:=true;
    Edit1.Text:='';
    MaskEdit1.Visible:=false;
    MaskEdit2.Visible:=false;
    Label5.Visible:=false;
    Label4.Caption:='Informação:';
    Label4.Left:=4;
    Button1.Left:=288;

  End;
//Implementação na opção "Entre Datas"
if(RadioGroup1.ItemIndex=4)THEN
  Begin
    Edit1.Visible:=False;
    MaskEdit1.Visible:=true;
    MaskEdit1.Text:='';
    MaskEdit2.Visible:=true;
    MaskEdit2.Text:='';
    Label5.Visible:=true;
    Label4.Caption:='Entre:';
    Label4.Left:=45;
    Button1.Left:=327;

  End;

  end;//fim procedure

end.
