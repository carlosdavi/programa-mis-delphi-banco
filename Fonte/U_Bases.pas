unit U_Bases;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, StdCtrls, DBCtrls, Mask, ComCtrls,
  Buttons, ToolWin, DBActns, ActnList, System.Actions;

type
  TF_Cadastro_Bases = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton1: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    BitBtn7: TBitBtn;
    ToolButton6: TToolButton;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    BitBtn10: TBitBtn;
    ToolButton9: TToolButton;
    ActionList1: TActionList;
    DataSetInsert1: TDataSetInsert;
    DataSetDelete1: TDataSetDelete;
    DataSetEdit1: TDataSetEdit;
    DataSetPost1: TDataSetPost;
    DataSetCancel1: TDataSetCancel;
    DataSetFirst1: TDataSetFirst;
    DataSetPrior1: TDataSetPrior;
    DataSetNext1: TDataSetNext;
    DataSetLast1: TDataSetLast;
    ToolButton10: TToolButton;
    BitBtn11: TBitBtn;
    GroupBox1: TGroupBox;
    DBText1: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit2: TDBEdit;
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure DBComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit2KeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit4KeyPress(Sender: TObject; var Key: Char);
    procedure DataSetCancel1Execute(Sender: TObject);
    procedure DataSetPost1Execute(Sender: TObject);
    procedure DataSetInsert1Execute(Sender: TObject);
    procedure DataSetEdit1Execute(Sender: TObject);
    procedure DataSetDelete1Execute(Sender: TObject);
    procedure DataSetFirst1Execute(Sender: TObject);
    procedure DataSetPrior1Execute(Sender: TObject);
    procedure DataSetNext1Execute(Sender: TObject);
    procedure DataSetLast1Execute(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Cadastro_Bases: TF_Cadastro_Bases;

implementation

uses U_dados;

{$R *.dfm}

procedure TF_Cadastro_Bases.BitBtn5Click(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Bases.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Bases.BitBtn8Click(Sender: TObject);
begin
F_dados.Q_Bases.Last;
end;

procedure TF_Cadastro_Bases.BitBtn9Click(Sender: TObject);
begin
if(application.MessageBox('Tem Certeza que deseja Fechar a Janela?','Fechamento de Janela',MB_YESNO)=IDYES) THEN
Close;
end;

procedure TF_Cadastro_Bases.FormKeyPress(Sender: TObject;
var Key: Char );
begin
  //Fechando Janela Caso ESC pressionado
  if (key=#27) then
    Begin
      close;
    End;

//Deixando Letra Grande
  Key:=UPCASE(key);


  //Trocar as letras Acentuadas por Letras sem acentos

if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';

if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';


if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';

if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';

if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';

if(key='�') OR (key='�') Then key:='C';
if(key='�') OR (key='�') THEN Key:='N';
if(key='�') OR (key='�') THEN Key:='Y';

//Ativando o ENTER, fazendo com que ao pressionar o enter ele pule de um campo para outro
if (key=#13) then
selectnext(activecontrol,true,true);
//end;
end;

procedure TF_Cadastro_Bases.DBEdit1KeyPress(Sender: TObject;
  var Key: Char);
begin
if (key=#13) THEN DBEdit5.SetFocus;
end;

procedure TF_Cadastro_Bases.DBComboBox1KeyPress(Sender: TObject;
  var Key: Char);
begin
if (key=#13) THEN DBEdit2.SetFocus;
end;

procedure TF_Cadastro_Bases.DBEdit2KeyPress(Sender: TObject;
  var Key: Char);
begin
if(key=#13) THEN DBEdit9.SetFocus;
end;

procedure TF_Cadastro_Bases.DBEdit4KeyPress(Sender: TObject;
  var Key: Char);
begin
if(key=#13) THEN DBEdit5.SetFocus;
end;

procedure TF_Cadastro_Bases.DataSetCancel1Execute(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Bases.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End; 
end;

procedure TF_Cadastro_Bases.DataSetPost1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
  F_dados.Q_Bases.Post;
  F_dados.Q_Bases.Close; //fecha tabela
  F_dados.Q_Bases.Open; // abre tabela resultando em  Atualiza Tabela
  ShowMessage('Registro Salvo!');
  DBEdit1.SetFocus;
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Bases.DataSetInsert1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
  F_dados.Q_Bases.Insert;
  DBEdit1.SetFocus;
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Bases.DataSetEdit1Execute(Sender: TObject);
begin
try //Alterando Registros
  Begin

  F_dados.Q_Bases.Edit;
  ShowMessage('Opera��o de Edi��o Aberta!');
  End;
  Except
   ShowMessage('N�o foi poss�vel alterar os dados!');
  End;
end;

procedure TF_Cadastro_Bases.DataSetDelete1Execute(Sender: TObject);
begin
try //Excluindo Registros
  Begin
    if(application.MessageBox('Deseja Excluir os Dados?','Confirma��o de Exclus�o',MB_YESNO)=IDYES) THEN
    Begin
        F_dados.Q_Bases.Delete;
        ShowMessage('Dados Excluidos com Sucesso!');
    End;
  End;
  Except
   ShowMessage('N�o foi poss�vel fazer a exclus�o!');
  End;
end;

procedure TF_Cadastro_Bases.DataSetFirst1Execute(Sender: TObject);
begin
F_dados.Q_Bases.First;
end;

procedure TF_Cadastro_Bases.DataSetPrior1Execute(Sender: TObject);
begin
F_dados.Q_Bases.Prior;
end;

procedure TF_Cadastro_Bases.DataSetNext1Execute(Sender: TObject);
begin
F_dados.Q_Bases.Next;
end;

procedure TF_Cadastro_Bases.DataSetLast1Execute(Sender: TObject);
begin
F_dados.Q_Bases.Last;
end;

procedure TF_Cadastro_Bases.BitBtn11Click(Sender: TObject);
begin
//Atualiza��o de Tabela
F_Dados.Q_Bases.Close;
F_Dados.Q_Bases.Open;
end;

procedure TF_Cadastro_Bases.FormActivate(Sender: TObject);
begin
//Atualizando Tabela ao Abrir
F_Dados.Q_Bases.Close;
F_Dados.Q_Bases.SQL.Clear; //limpa sql
F_dados.Q_Bases.SQL.Add('select * from bases'); //tras sql padr�o
F_Dados.Q_Bases.Open;


end;

end.
