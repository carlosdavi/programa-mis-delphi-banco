unit U_Pesquisa_Pastor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, ExtCtrls, Mask;

type
  TF_Consulta_Pastor = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    MaskEdit1: TMaskEdit;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure MaskEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    codAcesso:Integer;
  end;

var
  F_Consulta_Pastor: TF_Consulta_Pastor;

implementation

uses U_dados, ADODB, U_Discipulos, U_Geracao;

{$R *.dfm}

procedure TF_Consulta_Pastor.BitBtn2Click(Sender: TObject);
begin
//------------------------------------------------------
//-- Implementacao da tela de discipulos
if(codAcesso=1) THEN
  BEGIN
    F_Cadastro_Discipulos.DBText3.Caption:=F_dados.Q_Visao_PesquisaRapida_Pastoresnome_pastor.value;
    F_dados.Q_Discipuloscodpastor.Value:=F_dados.Q_Visao_PesquisaRapida_Pastorescodpastor.Value;//Nome_Pastor

  End
//-- Fim da implementacao da tela de discipulos
//-------------------------------------------------------------

//-------------------------------------------------------------
// -- IMPLEMENTAC�O DA TELA DE CADASTRO DE GERACAO
else if(codAcesso=5) THEN
  BEGIN //nome do pastor
    F_Cadastro_Geracao.DBText5.Caption:=F_Dados.Q_Visao_PesquisaRapida_Pastoresnome_pastor.Value;
   // F_dados.Q_Visao_Geracoesnome_pastor.Value:=F_Cadastro_Geracao.DBText5.Caption;
    //cod do pastor
    F_Cadastro_Geracao.DBText6.Caption:=INTtoSTR(F_Dados.Q_Visao_PesquisaRapida_Pastorescodpastor.Value);
    //F_dados.Q_Visao_Geracoescodpastor.Value:=F_Dados.Q_Visao_PesquisaRapida_Pastorescodpastor.Value;
   END;
// -- FIM IMPLEMENTAAO DA TELA DE CADADTRO DE GERACAO
//--------------------------------------------------------------
CLOSE; // --Fecha a Janela apos clica no botao
end;

procedure TF_Consulta_Pastor.BitBtn1Click(Sender: TObject);
begin
//Configura��o do Bot�o

//Configura��o do Bot�o
    WITH F_dados.Q_Visao_PesquisaRapida_Pastores DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_PesquisaRapida_Pastores where nome_pastor like :N');
        Parameters[0].Value:='%'+MaskEdit1.Text+'%';
      //abrir o q_Turmas
        Open;
       END;


end; //Fim M�todo

procedure TF_Consulta_Pastor.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
//configurando para executar ao apertar enter

    if(KEY=#13) THEN
    Begin
    BitBtn1.Click; // vai dar um clique no Bot�o
    End;
end;

procedure TF_Consulta_Pastor.MaskEdit1KeyPress(Sender: TObject;
  var Key: Char);
begin
 //configurando para executar ao apertar enter

    if(KEY=#13) THEN
    Begin
    BitBtn1.Click; // vai dar um clique no Bot�o
    End;
end;

procedure TF_Consulta_Pastor.RadioGroup1Click(Sender: TObject);
begin
//Condi��o para alterar a formaa��o do texto em data


end;

end.
