unit U_Lista_Geracoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ToolWin,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TF_Lista_Geracoes = class(TForm)
    DBGrid1: TDBGrid;
    ToolBar1: TToolBar;
    BitBtn1: TBitBtn;
    ToolBar2: TToolBar;
    Label1: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
     codAcesso:Integer;
  end;

var
  F_Lista_Geracoes: TF_Lista_Geracoes;

implementation

{$R *.dfm}

uses U_Dados, U_Consulta_Discipulos, U_Controle_Lideres, U_Relatorio_Celula;

procedure TF_Lista_Geracoes.BitBtn1Click(Sender: TObject);
begin
//implementação de consulta_discipulos
if(codAcesso=1)THEN
  Begin
    F_Consulta_Discipulos.MaskEdit1.Text:=F_dados.Q_GeracoesnomeGeracao.Value;
  End;
//implementação de controle de lideres
if(codAcesso=2)THEN
  Begin
   F_Controle_Lideres.MaskEdit1.Text:=F_dados.Q_GeracoesnomeGeracao.Value;
  End;
  //implementação de relatorio de celulas
if(codAcesso=3)THEN
  Begin
   F_Relatorio_Celula.Edit1.Text:=F_dados.Q_GeracoesnomeGeracao.Value;
  End;
F_Lista_Geracoes.Close;

end;//fim procedimento

procedure TF_Lista_Geracoes.FormActivate(Sender: TObject);
begin
//esse trecho de codigo foi posto com o intuido de evitar bugs de consulta
WITH F_dados.Q_Geracoes DO
  Begin
       close;
       sql.Clear;
       sql.Add('select * from geracoes');
       open;
  End;

end;//fim procedure

end.
