unit U_Discipulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, StdCtrls, DBCtrls, Mask, ComCtrls,
  Buttons, ToolWin, DBActns, ActnList, System.Actions;

type
  TF_Cadastro_Discipulos = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton1: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    BitBtn7: TBitBtn;
    ToolButton6: TToolButton;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    BitBtn10: TBitBtn;
    ToolButton9: TToolButton;
    ActionList1: TActionList;
    DataSetInsert1: TDataSetInsert;
    DataSetDelete1: TDataSetDelete;
    DataSetEdit1: TDataSetEdit;
    DataSetPost1: TDataSetPost;
    DataSetCancel1: TDataSetCancel;
    DataSetFirst1: TDataSetFirst;
    DataSetPrior1: TDataSetPrior;
    DataSetNext1: TDataSetNext;
    DataSetLast1: TDataSetLast;
    ToolButton10: TToolButton;
    BitBtn11: TBitBtn;
    GroupBox1: TGroupBox;
    DBText1: TDBText;
    Label1: TLabel;
    Label4: TLabel;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    Label14: TLabel;
    DBEdit13: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    DBComboBox3: TDBComboBox;
    Label18: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit5: TDBEdit;
    Label5: TLabel;
    DBComboBox1: TDBComboBox;
    Label3: TLabel;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    Label12: TLabel;
    DBEdit4: TDBEdit;
    DBComboBox2: TDBComboBox;
    Label13: TLabel;
    Label15: TLabel;
    DBComboBox4: TDBComboBox;
    DBEdit9: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBMemo1: TDBMemo;
    Label19: TLabel;
    DBLookupComboBox3: TDBLookupComboBox;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    DBText2: TDBText;
    DBText3: TDBText;
    Label20: TLabel;
    DBText4: TDBText;
    BitBtn14: TBitBtn;
    Label21: TLabel;
    DBText5: TDBText;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    BitBtn19: TBitBtn;
    Label22: TLabel;
    DBEdit3: TDBEdit;
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure DBComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit3KeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit4KeyPress(Sender: TObject; var Key: Char);
    procedure DataSetCancel1Execute(Sender: TObject);
    procedure DataSetPost1Execute(Sender: TObject);
    procedure DataSetInsert1Execute(Sender: TObject);
    procedure DataSetEdit1Execute(Sender: TObject);
    procedure DataSetDelete1Execute(Sender: TObject);
    procedure DataSetFirst1Execute(Sender: TObject);
    procedure DataSetPrior1Execute(Sender: TObject);
    procedure DataSetNext1Execute(Sender: TObject);
    procedure DataSetLast1Execute(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure DBEdit10KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Cadastro_Discipulos: TF_Cadastro_Discipulos;

implementation

uses U_dados, U_Pesquisa_Lider, U_Pesquisa_Pastor, U_Selecao_de_Rede,
  U_Selecao_de_Geracao;

{$R *.dfm}

procedure TF_Cadastro_Discipulos.BitBtn5Click(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Discipulos.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Discipulos.BitBtn8Click(Sender: TObject);
begin
F_dados.Q_Discipulos.Last;
end;

procedure TF_Cadastro_Discipulos.BitBtn9Click(Sender: TObject);
begin
if(application.MessageBox('Tem Certeza que deseja Fechar a Janela?','Fechamento de Janela',MB_YESNO)=IDYES) THEN
Close;
end;

procedure TF_Cadastro_Discipulos.FormKeyPress(Sender: TObject;
var Key: Char );
begin
  //Fechando Janela Caso ESC pressionado
  if (key=#27) then
    Begin
      close;
    End;

//Deixando Letra Grande
  Key:=UPCASE(key);


  //Trocar as letras Acentuadas por Letras sem acentos

if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';

if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';


if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';

if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';

if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';

if(key='�') OR (key='�') Then key:='C';
if(key='�') OR (key='�') THEN Key:='N';
if(key='�') OR (key='�') THEN Key:='Y';

//Ativando o ENTER, fazendo com que ao pressionar o enter ele pule de um campo para outro
if (key=#13) then
selectnext(activecontrol,true,true);
//end;
end;

procedure TF_Cadastro_Discipulos.DBEdit1KeyPress(Sender: TObject;
  var Key: Char);
begin
if (key=#13) THEN DBCOMBOBOX1.SetFocus;
end;

procedure TF_Cadastro_Discipulos.DBComboBox1KeyPress(Sender: TObject;
  var Key: Char);
begin
if (key=#13) THEN DBEdit5.SetFocus;
end;

procedure TF_Cadastro_Discipulos.DBEdit3KeyPress(Sender: TObject;
  var Key: Char);
begin
if(key=#13) THEN DBEdit4.SetFocus;
end;

procedure TF_Cadastro_Discipulos.DBEdit4KeyPress(Sender: TObject;
  var Key: Char);
begin
if(key=#13) THEN DBEdit5.SetFocus;
end;

procedure TF_Cadastro_Discipulos.DataSetCancel1Execute(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Discipulos.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End; 
end;

procedure TF_Cadastro_Discipulos.DataSetPost1Execute(Sender: TObject);
var
codDiscipulo:Integer;
begin
//Inserindo Registros
try
  Begin
//inserindo manualmente os campos: Nome_Lider e Nome_Pastor
//F_dados.Q_Discipuloscodlider.Value:=F_dados.Q_Visao_PesquisaRapida_Liderescodlider.Value; //Nome_Lider
//F_dados.Q_Discipuloscodpastor.Value:=F_dados.Q_Visao_PesquisaRapida_Pastorescodpastor.Value;//Nome_Pastor

//--- Fim insercoes
  F_dados.Q_Discipulos.Post;
  F_dados.Q_Discipulos.Close; //fecha tabela
  F_dados.Q_Discipulos.Open; // abre tabela resultando em  Atualiza Tabela
  ShowMessage('Registro Salvo!');
  DBEdit3.Enabled:=false; //desabilita o situa��o para evitar bugs
  DBEdit1.SetFocus;
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;

 //implementa��o de tratamento para o primeiro discipulo registrado
  //fazendo dele lider de si mesmo, para evitar bug
  if(F_dados.Q_Verifica_VazioCOLUMN1.Value-1=0)THEN
    Begin
      ShowMessage('Esse registro ser� setado como lider e pastor de s�, pois � o primeiro!');
      WITH F_dados.Q_Discipulos DO
        Begin
          close;
          sql.clear;
          sql.add('select * from discipulos where coddiscipulo=1');
          open;
          codDiscipulo:=F_Dados.Q_Discipuloscoddiscipulo.Value;
        End;
        F_dados.Q_Discipulos.Edit; //inicia altera��o
        F_Dados.Q_Discipuloscodlider.Value:=codDiscipulo;
        F_Dados.Q_Discipuloscodpastor.Value:=codDiscipulo;
        F_dados.Q_Discipulos.Post; //salva altera��o
        //Agora reseto a sql para evitar bugs
        WITH F_dados.Q_Discipulos DO
        Begin
          close;
          sql.clear;
          sql.add('select * from discipulos');
          open;
        End;
    End;

end;  //fim procedure

procedure TF_Cadastro_Discipulos.DataSetInsert1Execute(Sender: TObject);
begin


//Inserindo Registros
try
  Begin
  F_dados.Q_Discipulos.Insert;
  DBEdit1.SetFocus;
  //Por Padr�o o discipulo � considerado Ativo, isso porem pode ser mudado posteriormente
  DBEdit3.Enabled:=False;
  DBEdit3.Text:='ATIVO';
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Discipulos.DataSetEdit1Execute(Sender: TObject);
begin
try //Alterando Registros
  Begin

  F_dados.Q_Discipulos.Edit;
  DBEdit3.Enabled:=true;
  ShowMessage('Opera��o de Edi��o Aberta!');
  End;
  Except
   ShowMessage('N�o foi poss�vel alterar os dados!');
  End;
end;

procedure TF_Cadastro_Discipulos.DataSetDelete1Execute(Sender: TObject);
begin
try //Excluindo Registros
  Begin
    if(application.MessageBox('Deseja Excluir os Dados?','Confirma��o de Exclus�o',MB_YESNO)=IDYES) THEN
    Begin
        F_dados.Q_Discipulos.Delete;
        ShowMessage('Dados Excluidos com Sucesso!');
    End;
  End;
  Except
   ShowMessage('N�o foi poss�vel fazer a exclus�o!');
  End;
end;

procedure TF_Cadastro_Discipulos.DataSetFirst1Execute(Sender: TObject);
begin
F_dados.Q_Discipulos.First;
end;

procedure TF_Cadastro_Discipulos.DataSetPrior1Execute(Sender: TObject);
begin
F_dados.Q_Discipulos.Prior;
end;

procedure TF_Cadastro_Discipulos.DataSetNext1Execute(Sender: TObject);
begin
F_dados.Q_Discipulos.Next;
end;

procedure TF_Cadastro_Discipulos.DataSetLast1Execute(Sender: TObject);
begin
F_dados.Q_Discipulos.Last;
end;

procedure TF_Cadastro_Discipulos.BitBtn11Click(Sender: TObject);
begin
//Atualiza��o de Tabela
F_Dados.Q_Discipulos.Close;
F_Dados.Q_Discipulos.Open;
end;

procedure TF_Cadastro_Discipulos.FormActivate(Sender: TObject);
begin

//Limpando Campos Nome_Pastor e Nome_Lider ao Abrir
DBText2.Caption:='---';
DBText3.Caption:='---';

//Atualizando Tabela ao Abrir
F_Dados.Q_Discipulos.Close;
F_Dados.Q_Discipulos.SQL.Clear; //limpa sql
F_dados.Q_Discipulos.SQL.Add('select * from discipulos'); //tras sql padr�o
F_Dados.Q_Discipulos.Open;


end;

procedure TF_Cadastro_Discipulos.BitBtn12Click(Sender: TObject);
begin
 F_Consulta_Lider.codAcesso:=1;
F_Consulta_Lider.ShowModal;
end;

procedure TF_Cadastro_Discipulos.BitBtn13Click(Sender: TObject);
begin
F_Consulta_Pastor.codAcesso:=1;
F_Consulta_Pastor.ShowModal;
end;

procedure TF_Cadastro_Discipulos.DBEdit10KeyPress(Sender: TObject;
  var Key: Char);
begin
if key=#9 THEN DBEdit13.SetFocus;
end;

procedure TF_Cadastro_Discipulos.BitBtn14Click(Sender: TObject);
begin
//-------
F_Consultar_Rede.codAcesso:=1;
F_Consultar_Rede.ShowModal;
end;

procedure TF_Cadastro_Discipulos.BitBtn15Click(Sender: TObject);
begin
 F_Consultar_Geracao.codAcesso:=1;
F_Consultar_Geracao.ShowModal;
end;

procedure TF_Cadastro_Discipulos.BitBtn16Click(Sender: TObject);
begin
F_Cadastro_Discipulos.DBText2.Caption:='--';
F_dados.Q_Discipuloscodlider.Value:=0;
end;

procedure TF_Cadastro_Discipulos.BitBtn17Click(Sender: TObject);
begin
F_Cadastro_Discipulos.DBText3.Caption:='--';
    F_dados.Q_Discipuloscodpastor.Value:=0;

end;

procedure TF_Cadastro_Discipulos.BitBtn18Click(Sender: TObject);
begin
F_Cadastro_Discipulos.DBText4.Caption:='--';
    F_dados.Q_DiscipulosredeDiscipulo.Value:='';
    F_dados.Q_Discipuloscodrede.Value:=0;
end;

procedure TF_Cadastro_Discipulos.BitBtn19Click(Sender: TObject);
begin
F_Cadastro_Discipulos.DBText5.Caption:='--';
    F_dados.Q_Discipuloscodgeracao.Value:=0;
end;

end.
