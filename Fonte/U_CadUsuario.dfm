object F_CadUsuario: TF_CadUsuario
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Cadastro de Usuarios'
  ClientHeight = 520
  ClientWidth = 705
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 92
    Width = 107
    Height = 20
    Caption = 'Nome de Usu'#225'rio'
    FocusControl = DBEdit1
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 278
    Top = 92
    Width = 39
    Height = 20
    Caption = 'Senha'
    FocusControl = DBEdit2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 705
    Height = 41
    Align = alTop
    Caption = 'Cadastro de Usu'#225'rios'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Rockwell Condensed'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 41
    Width = 705
    Height = 40
    ButtonHeight = 37
    Caption = 'ToolBar1'
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 0
      Top = 0
      Width = 97
      Height = 37
      Caption = 'Novo Usu'#225'rio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 97
      Top = 0
      Width = 97
      Height = 37
      Caption = 'Salvar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 194
      Top = 0
      Width = 97
      Height = 37
      Caption = 'Editar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn3Click
    end
    object BitBtn4: TBitBtn
      Left = 291
      Top = 0
      Width = 97
      Height = 37
      Caption = 'Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BitBtn4Click
    end
    object BitBtn5: TBitBtn
      Left = 388
      Top = 0
      Width = 97
      Height = 37
      Caption = 'Fechar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = BitBtn5Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 411
    Width = 705
    Height = 109
    Align = alBottom
    DataSource = F_Dados.D_Usuarios
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 138
    Width = 191
    Height = 154
    Caption = 'Cadastros'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object DBCheckBox1: TDBCheckBox
      Left = 8
      Top = 22
      Width = 174
      Height = 17
      Caption = 'Cadastro de Disc'#237'pulos'
      DataField = 'CadDiscipulo'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 0
    end
    object DBCheckBox2: TDBCheckBox
      Left = 8
      Top = 38
      Width = 145
      Height = 17
      Caption = 'Cadastro de Territorios'
      DataField = 'CadTerritorio'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 1
    end
    object DBCheckBox3: TDBCheckBox
      Left = 8
      Top = 54
      Width = 174
      Height = 17
      Caption = 'Cadastro de Apostolos'
      DataField = 'CadApostulo'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 2
    end
    object DBCheckBox4: TDBCheckBox
      Left = 8
      Top = 70
      Width = 180
      Height = 17
      Caption = 'Cadastro de 12 Primeira G.'
      DataField = 'Cad12PrimeiraG'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 3
    end
    object DBCheckBox5: TDBCheckBox
      Left = 8
      Top = 86
      Width = 145
      Height = 17
      Caption = 'Cadastro de Rede'
      DataField = 'CadRede'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 4
    end
    object DBCheckBox6: TDBCheckBox
      Left = 8
      Top = 102
      Width = 145
      Height = 17
      Caption = 'Cadastro de Lider'
      DataField = 'CadLider'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 5
    end
    object DBCheckBox7: TDBCheckBox
      Left = 8
      Top = 118
      Width = 161
      Height = 17
      Caption = 'Cadastro de Colider'
      DataField = 'CadColider'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 6
    end
    object DBCheckBox8: TDBCheckBox
      Left = 8
      Top = 134
      Width = 145
      Height = 17
      Caption = 'Cadastro de Gera'#231#227'o'
      DataField = 'CadGeracao'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 7
    end
  end
  object GroupBox2: TGroupBox
    Left = 204
    Top = 139
    Width = 185
    Height = 150
    Caption = 'Consultas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object DBCheckBox11: TDBCheckBox
      Left = 8
      Top = 18
      Width = 153
      Height = 17
      Caption = 'Consulta de Discipulos'
      DataField = 'ConsultaDiscipulo'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 0
    end
    object DBCheckBox12: TDBCheckBox
      Left = 8
      Top = 34
      Width = 145
      Height = 17
      Caption = 'Controle Lideres'
      DataField = 'ControleLideres'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 1
    end
  end
  object GroupBox3: TGroupBox
    Left = 394
    Top = 139
    Width = 185
    Height = 146
    Caption = 'Multiplica'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    object DBCheckBox9: TDBCheckBox
      Left = 6
      Top = 34
      Width = 131
      Height = 17
      Caption = 'Multiplicar C'#233'lula'
      DataField = 'MultiplicarCelula'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 0
    end
    object DBCheckBox10: TDBCheckBox
      Left = 6
      Top = 18
      Width = 123
      Height = 17
      Caption = 'Alocar Discipulo'
      DataField = 'AlocarDiscipulo'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 1
    end
  end
  object GroupBox4: TGroupBox
    Left = 8
    Top = 298
    Width = 185
    Height = 105
    Caption = 'Relat'#243'rios'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    object DBCheckBox13: TDBCheckBox
      Left = 8
      Top = 22
      Width = 177
      Height = 17
      Caption = 'Relatorio Geral de C'#233'lulas'
      DataField = 'RelatorioGerCelula'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 0
    end
  end
  object GroupBox5: TGroupBox
    Left = 199
    Top = 298
    Width = 185
    Height = 105
    Caption = 'Controle de Usu'#225'rios '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    object DBCheckBox14: TDBCheckBox
      Left = 10
      Top = 22
      Width = 167
      Height = 17
      Caption = 'Cadastro de Usu'#225'rios'
      DataField = 'caduser'
      DataSource = F_Dados.D_Usuarios
      TabOrder = 0
    end
  end
  object DBEdit1: TDBEdit
    Left = 8
    Top = 112
    Width = 264
    Height = 21
    DataField = 'Nome_user'
    DataSource = F_Dados.D_Usuarios
    TabOrder = 8
  end
  object DBEdit2: TDBEdit
    Left = 278
    Top = 112
    Width = 199
    Height = 21
    DataField = 'Senha'
    DataSource = F_Dados.D_Usuarios
    TabOrder = 9
  end
end
