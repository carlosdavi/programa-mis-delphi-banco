program Projeto;

uses
  Forms,
  U_Login in 'U_Login.pas' {F_Login},
  U_Menu in 'U_Menu.pas' {F_Menu},
  U_Dados in 'U_Dados.pas' {F_Dados: TDataModule},
  U_Bases in 'U_Bases.pas' {F_Cadastro_Bases},
  U_Discipulos in 'U_Discipulos.pas' {F_Cadastro_Discipulos},
  U_Apostulos in 'U_Apostulos.pas' {F_Cadastro_Apostulo},
  U_Pastores in 'U_Pastores.pas' {F_Cadastro_Pastores},
  U_Lideres in 'U_Lideres.pas' {F_Cadastro_Lideres},
  U_CoLider in 'U_CoLider.pas' {F_Cadastro_Co_Lider},
  U_MultiplicacaoCelula in 'U_MultiplicacaoCelula.pas' {F_Multiplicacao_Celula},
  U_Consulta_Discipulos in 'U_Consulta_Discipulos.pas' {F_Consulta_Discipulos},
  U_Pesquisa_Lider in 'U_Pesquisa_Lider.pas' {F_Consulta_Lider},
  U_Pesquisa_Pastor in 'U_Pesquisa_Pastor.pas' {F_Consulta_Pastor},
  U_Rede in 'U_Rede.pas' {F_Cadastro_Rede},
  U_Controle_Lideres in 'U_Controle_Lideres.pas' {F_Controle_Lideres},
  U_Geracao in 'U_Geracao.pas' {F_Cadastro_Geracao},
  Vcl.Themes,
  Vcl.Styles,
  U_Selecao_de_Geracao in 'U_Selecao_de_Geracao.pas' {F_Consultar_Geracao},
  U_Selecao_de_Rede in 'U_Selecao_de_Rede.pas' {F_Consultar_Rede},
  U_Lista_Geracoes in 'U_Lista_Geracoes.pas' {F_Lista_Geracoes},
  U_Relatorio_Celula in 'U_Relatorio_Celula.pas' {F_Relatorio_Celula},
  U_Relatorio_Geral_Celula in 'U_Relatorio_Geral_Celula.pas' {F_Relatorio_Geral_Celula},
  U_Alocacao_Discipulo in 'U_Alocacao_Discipulo.pas' {F_Alocacao_Discipulo},
  U_CadUsuario in 'U_CadUsuario.pas' {F_CadUsuario};

{$R *.res}

begin
  Application.Initialize;
  TStyleManager.TrySetStyle('Amethyst Kamri');
  Application.CreateForm(TF_Login, F_Login);
  Application.CreateForm(TF_Menu, F_Menu);
  Application.CreateForm(TF_Dados, F_Dados);
  Application.CreateForm(TF_Cadastro_Bases, F_Cadastro_Bases);
  Application.CreateForm(TF_Cadastro_Discipulos, F_Cadastro_Discipulos);
  Application.CreateForm(TF_Cadastro_Apostulo, F_Cadastro_Apostulo);
  Application.CreateForm(TF_Cadastro_Pastores, F_Cadastro_Pastores);
  Application.CreateForm(TF_Cadastro_Lideres, F_Cadastro_Lideres);
  Application.CreateForm(TF_Cadastro_Co_Lider, F_Cadastro_Co_Lider);
  Application.CreateForm(TF_Multiplicacao_Celula, F_Multiplicacao_Celula);
  Application.CreateForm(TF_Consulta_Discipulos, F_Consulta_Discipulos);
  Application.CreateForm(TF_Consulta_Lider, F_Consulta_Lider);
  Application.CreateForm(TF_Consulta_Pastor, F_Consulta_Pastor);
  Application.CreateForm(TF_Cadastro_Rede, F_Cadastro_Rede);
  Application.CreateForm(TF_Controle_Lideres, F_Controle_Lideres);
  Application.CreateForm(TF_Cadastro_Geracao, F_Cadastro_Geracao);
  Application.CreateForm(TF_Consultar_Geracao, F_Consultar_Geracao);
  Application.CreateForm(TF_Consultar_Rede, F_Consultar_Rede);
  Application.CreateForm(TF_Lista_Geracoes, F_Lista_Geracoes);
  Application.CreateForm(TF_Relatorio_Celula, F_Relatorio_Celula);
  Application.CreateForm(TF_Relatorio_Geral_Celula, F_Relatorio_Geral_Celula);
  Application.CreateForm(TF_Alocacao_Discipulo, F_Alocacao_Discipulo);
  Application.CreateForm(TF_CadUsuario, F_CadUsuario);
  // Application.CreateForm(TF_Lista_Geracoes, F_Lista_Geracoes);
  // Application.CreateForm(TF_Consultar_Rede1, F_Consultar_Rede1);
  Application.CreateForm(TF_Consultar_Rede, F_Consultar_Rede);
  Application.Run;
end.
