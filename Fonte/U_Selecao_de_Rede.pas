unit U_Selecao_de_Rede;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, StdCtrls, DBCtrls, Mask, ComCtrls,
  Buttons, ToolWin, DBActns, ActnList, System.Actions;

type
  TF_Consultar_Rede = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    BitBtn9: TBitBtn;
    ActionList1: TActionList;
    DataSetInsert1: TDataSetInsert;
    DataSetDelete1: TDataSetDelete;
    DataSetEdit1: TDataSetEdit;
    DataSetPost1: TDataSetPost;
    DataSetCancel1: TDataSetCancel;
    DataSetFirst1: TDataSetFirst;
    DataSetPrior1: TDataSetPrior;
    DataSetNext1: TDataSetNext;
    DataSetLast1: TDataSetLast;
    ToolButton1: TToolButton;
    BitBtn1: TBitBtn;
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DataSetCancel1Execute(Sender: TObject);
    procedure DataSetPost1Execute(Sender: TObject);
    procedure DataSetInsert1Execute(Sender: TObject);
    procedure DataSetEdit1Execute(Sender: TObject);
    procedure DataSetDelete1Execute(Sender: TObject);
    procedure DataSetFirst1Execute(Sender: TObject);
    procedure DataSetPrior1Execute(Sender: TObject);
    procedure DataSetNext1Execute(Sender: TObject);
    procedure DataSetLast1Execute(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    codAcesso:Integer;
  end;

var
  F_Consultar_Rede: TF_Consultar_Rede;

implementation

uses U_dados, U_Discipulos, U_Geracao;

{$R *.dfm}

procedure TF_Consultar_Rede.BitBtn5Click(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Pastores.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End;
end;

procedure TF_Consultar_Rede.BitBtn8Click(Sender: TObject);
begin
F_dados.Q_Pastores.Last;
end;

procedure TF_Consultar_Rede.BitBtn9Click(Sender: TObject);
begin
//if(application.MessageBox('Tem Certeza que deseja Fechar a Janela?','Fechamento de Janela',MB_YESNO)=IDYES) THEN
//----------------------------------------------------
//implementacao para a tela de discipulos
if(codAcesso=1)THEN
  Begin //inserindo campos de cod rede no DBTEXT e cod na tabela
    F_Cadastro_Discipulos.DBText4.Caption:=F_Dados.Q_RedenomeRede.Value;
    F_dados.Q_DiscipulosredeDiscipulo.Value:=F_Dados.Q_RedenomeRede.Value;
    F_dados.Q_Discipuloscodrede.Value:=F_dados.Q_Redecodrede.Value;
  End

//-- implementacao da tela de Cadastro de Geracao (Campo Rede)
else if(codAcesso=7) THEN
  Begin
   //implementacao do Nome
   F_Cadastro_Geracao.DBText9.Caption:=F_dados.Q_RedenomeRede.Value; //mostra na tela
  // F_Dados.Q_Visao_GeracoesnomeRede.Value:=F_dados.Q_RedenomeRede.Value; //insere no banco
  //implementacao do Codigo
    F_Cadastro_Geracao.DBText8.Caption:=IntToStr(F_dados.Q_Redecodrede.Value);//mostra na tela
    //F_Dados.Q_Visao_Geracoescodrede.Value:=F_dados.Q_Redecodrede.Value;//insere no banco
    F_Cadastro_Geracao.DBEdit2.Text:=IntToStr(F_dados.Q_Redecodrede.Value);
  End;
//-----------------------------------------------------
Close;


end;

procedure TF_Consultar_Rede.FormKeyPress(Sender: TObject;
var Key: Char );
begin
  //Fechando Janela Caso ESC pressionado
  if (key=#27) then
    Begin
      close;
    End;

//Deixando Letra Grande
  Key:=UPCASE(key);


  //Trocar as letras Acentuadas por Letras sem acentos

if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';

if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';


if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';

if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';

if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';

if(key='�') OR (key='�') Then key:='C';
if(key='�') OR (key='�') THEN Key:='N';
if(key='�') OR (key='�') THEN Key:='Y';

//Ativando o ENTER, fazendo com que ao pressionar o enter ele pule de um campo para outro
if (key=#13) then
selectnext(activecontrol,true,true);
//end;
end;

procedure TF_Consultar_Rede.DataSetCancel1Execute(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Pastores.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End; 
end;

procedure TF_Consultar_Rede.DataSetPost1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
  F_dados.Q_Pastores.Post;
  F_dados.Q_Pastores.Close; //fecha tabela
  F_dados.Q_Pastores.Open; // abre tabela resultando em  Atualiza Tabela
  ShowMessage('Registro Salvo!');
 
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Consultar_Rede.DataSetInsert1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
 // F_dados.Q_Lideres.Insert;
  //F_Dados.Q_Liderescodlider.Value:=STRtoINT(DBText1.Caption);
  //F_Dados.Q_LideresNome_Lider.Value:=DBText2.Caption;
  //F_Dados.Q_Liderescoddiscipulo.Value:=STRtoINT(DBText3.Caption);
  //F_Dados.Q_Lideres.Post;

  //ShowMessage('Registro Salvo!');
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Consultar_Rede.DataSetEdit1Execute(Sender: TObject);
begin
try //Alterando Registros
  Begin

  F_dados.Q_Lideres.Edit;
  ShowMessage('Opera��o de Edi��o Aberta!');
  End;
  Except
   ShowMessage('N�o foi poss�vel alterar os dados!');
  End;
end;

procedure TF_Consultar_Rede.DataSetDelete1Execute(Sender: TObject);
begin
try //Excluindo Registros
  Begin
    if(application.MessageBox('Deseja Excluir os Dados?','Confirma��o de Exclus�o',MB_YESNO)=IDYES) THEN
    Begin
        F_dados.Q_Lideres.Delete;
        ShowMessage('Dados Excluidos com Sucesso!');
    End;
  End;
  Except
   ShowMessage('N�o foi poss�vel fazer a exclus�o!');
  End;
end;

procedure TF_Consultar_Rede.DataSetFirst1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.First;
end;

procedure TF_Consultar_Rede.DataSetPrior1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Prior;
end;

procedure TF_Consultar_Rede.DataSetNext1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Next;
end;

procedure TF_Consultar_Rede.DataSetLast1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Last;
end;

procedure TF_Consultar_Rede.BitBtn11Click(Sender: TObject);
begin
//Atualiza��o de Tabela
F_Dados.Q_Lideres.Close;
F_Dados.Q_Lideres.Open;
end;

procedure TF_Consultar_Rede.FormActivate(Sender: TObject);
begin
//Atualizando Tabela ao Abrir
F_Dados.Q_Lideres.Close;
F_Dados.Q_Lideres.SQL.Clear; //limpa sql
F_dados.Q_Lideres.SQL.Add('select * from lideres'); //tras sql padr�o
F_Dados.Q_Lideres.Open;


end;

procedure TF_Consultar_Rede.BitBtn12Click(Sender: TObject);
begin
//Configura��o do Bot�o


end;

procedure TF_Consultar_Rede.BitBtn1Click(Sender: TObject);
begin
F_dados.Q_Rede.Close;
F_dados.Q_Rede.Open;

end;

end.
