object F_Relatorio_Geral_Celula: TF_Relatorio_Geral_Celula
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio Geral de C'#233'ulua'
  ClientHeight = 676
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 139
    Top = 93
    Width = 107
    Height = 20
    Caption = 'Selecione o L'#237'der'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 472
    Top = 93
    Width = 113
    Height = 20
    Caption = 'Selecione a C'#233'lula'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 41
    Align = alTop
    Caption = 'Relat'#243'rio Geral de Celula'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Bernard MT Condensed'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object DBGrid4: TDBGrid
    Left = 0
    Top = 568
    Width = 792
    Height = 108
    Align = alBottom
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 304
    Width = 792
    Height = 220
    Align = alBottom
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object GroupBox3: TGroupBox
      Left = 328
      Top = 22
      Width = 462
      Height = 196
      Align = alRight
      Caption = 'Vizualiza'#231#227'o do Relat'#243'rio:'
      TabOrder = 0
      object DBText1: TDBText
        Left = 91
        Top = 23
        Width = 116
        Height = 17
        DataField = 'anfitriaoCelula'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText2: TDBText
        Left = 91
        Top = 46
        Width = 65
        Height = 17
        DataField = 'horarioComeca'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText3: TDBText
        Left = 91
        Top = 65
        Width = 65
        Height = 17
        DataField = 'horarioTermino'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText4: TDBText
        Left = 91
        Top = 88
        Width = 65
        Height = 17
        DataField = 'dataCelula'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText5: TDBText
        Left = 91
        Top = 110
        Width = 368
        Height = 17
        DataField = 'enderecoCelula'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText6: TDBText
        Left = 91
        Top = 132
        Width = 358
        Height = 17
        DataField = 'Mensagem'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText7: TDBText
        Left = 91
        Top = 155
        Width = 364
        Height = 17
        DataField = 'observacoes'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText8: TDBText
        Left = 281
        Top = 25
        Width = 65
        Height = 17
        DataField = 'DisFixos'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText9: TDBText
        Left = 281
        Top = 46
        Width = 65
        Height = 17
        DataField = 'Visitantes'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText10: TDBText
        Left = 281
        Top = 65
        Width = 65
        Height = 17
        DataField = 'valorOferta'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText11: TDBText
        Left = 276
        Top = 88
        Width = 65
        Height = 17
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText12: TDBText
        Left = 401
        Top = 24
        Width = 65
        Height = 17
        DataField = 'DisPresentes'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object DBText13: TDBText
        Left = 401
        Top = 46
        Width = 65
        Height = 17
        DataField = 'Decisoes'
        DataSource = F_Dados.D_Rel_Celulas
      end
      object Label6: TLabel
        Left = 5
        Top = 24
        Width = 57
        Height = 20
        Caption = 'Anfitri'#227'o:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 5
        Top = 46
        Width = 56
        Height = 20
        Caption = 'H. In'#237'cio:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 5
        Top = 65
        Width = 73
        Height = 20
        Caption = 'H. T'#233'rmino:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 5
        Top = 87
        Width = 31
        Height = 20
        Caption = 'Data:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 5
        Top = 110
        Width = 63
        Height = 20
        Caption = 'Endere'#231'o:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 5
        Top = 133
        Width = 71
        Height = 20
        Caption = 'Mensagem:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 5
        Top = 155
        Width = 84
        Height = 20
        Caption = 'Observa'#231#245'es:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 213
        Top = 24
        Width = 65
        Height = 20
        Caption = 'Dis. Fixos:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 213
        Top = 46
        Width = 65
        Height = 20
        Caption = 'Visitantes:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 212
        Top = 65
        Width = 69
        Height = 20
        Caption = 'Val. Oferta:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 213
        Top = 87
        Width = 57
        Height = 20
        Caption = 'Anfitri'#227'o:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 333
        Top = 24
        Width = 65
        Height = 20
        Caption = 'Presentes:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 333
        Top = 46
        Width = 60
        Height = 20
        Caption = 'Decis'#245'es:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object GroupBox4: TGroupBox
      Left = 2
      Top = 22
      Width = 320
      Height = 196
      Align = alLeft
      Caption = 'Selecione o Relat'#243'rio a ser vizualizado:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object DBGrid3: TDBGrid
        Left = 2
        Top = 22
        Width = 316
        Height = 172
        Align = alClient
        DataSource = F_Dados.D_Rel_Celulas
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Arial Narrow'
        TitleFont.Style = []
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 524
    Width = 792
    Height = 44
    Align = alBottom
    TabOrder = 3
    object Label1: TLabel
      Left = 2
      Top = 15
      Width = 159
      Height = 27
      Align = alLeft
      Caption = 'Disc'#237'pulos da C'#233'lula:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitHeight = 23
    end
  end
  object GroupBox5: TGroupBox
    Left = 135
    Top = 41
    Width = 657
    Height = 55
    Align = alCustom
    TabOrder = 4
    object Label4: TLabel
      Left = 4
      Top = 24
      Width = 74
      Height = 20
      Caption = 'Informa'#231#227'o:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 198
      Top = 24
      Width = 9
      Height = 20
      Caption = 'E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Edit1: TEdit
      Left = 84
      Top = 19
      Width = 197
      Height = 28
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object MaskEdit1: TMaskEdit
      Left = 85
      Top = 19
      Width = 102
      Height = 28
      EditMask = '99/99/9999'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
      Visible = False
    end
    object MaskEdit2: TMaskEdit
      Left = 219
      Top = 19
      Width = 102
      Height = 28
      EditMask = '99/99/9999'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '  /  /    '
      Visible = False
    end
    object Button1: TButton
      Left = 288
      Top = 19
      Width = 75
      Height = 28
      Caption = 'Pesquisar'
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 0
    Top = 47
    Width = 129
    Height = 128
    Align = alCustom
    Caption = 'Filtro de Pesquisa'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    Items.Strings = (
      'Todos'
      'Nome do L'#237'der'
      'Gera'#231#227'o'
      'Territ'#243'rio'
      'Entre Datas')
    ParentFont = False
    TabOrder = 5
    OnClick = RadioGroup1Click
  end
  object DBGrid1: TDBGrid
    Left = 135
    Top = 112
    Width = 320
    Height = 192
    Align = alCustom
    DataSource = F_Dados.D_Discipulos
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    OnKeyDown = DBGrid1KeyDown
    OnKeyUp = DBGrid1KeyDown
  end
  object DBGrid2: TDBGrid
    Left = 463
    Top = 112
    Width = 320
    Height = 192
    DataSource = F_Dados.D_Celulas
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = DBGrid2CellClick
    OnColEnter = DBGrid2ColEnter
    OnKeyDown = DBGrid2KeyDown
    OnKeyUp = DBGrid2KeyDown
  end
end
