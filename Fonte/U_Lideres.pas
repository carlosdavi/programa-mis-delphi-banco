unit U_Lideres;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, StdCtrls, DBCtrls, Mask, ComCtrls,
  Buttons, ToolWin, DBActns, ActnList, System.Actions;

type
  TF_Cadastro_Lideres = class(TForm)
    Panel1: TPanel;
    ToolBar1: TToolBar;
    BitBtn6: TBitBtn;
    ToolButton2: TToolButton;
    BitBtn9: TBitBtn;
    ActionList1: TActionList;
    DataSetInsert1: TDataSetInsert;
    DataSetDelete1: TDataSetDelete;
    DataSetEdit1: TDataSetEdit;
    DataSetPost1: TDataSetPost;
    DataSetCancel1: TDataSetCancel;
    DataSetFirst1: TDataSetFirst;
    DataSetPrior1: TDataSetPrior;
    DataSetNext1: TDataSetNext;
    DataSetLast1: TDataSetLast;
    ToolButton10: TToolButton;
    BitBtn11: TBitBtn;
    GroupBox1: TGroupBox;
    DBText1: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    BitBtn12: TBitBtn;
    RadioGroup1: TRadioGroup;
    DBText2: TDBText;
    DBText3: TDBText;
    ToolButton1: TToolButton;
    Label5: TLabel;
    Panel2: TPanel;
    Label3: TLabel;
    Label6: TLabel;
    DBText4: TDBText;
    Label7: TLabel;
    Label11: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    Label9: TLabel;
    DBGrid1: TDBGrid;
    ComboBox1: TComboBox;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    MaskEdit1: TMaskEdit;
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DataSetCancel1Execute(Sender: TObject);
    procedure DataSetPost1Execute(Sender: TObject);
    procedure DataSetInsert1Execute(Sender: TObject);
    procedure DataSetEdit1Execute(Sender: TObject);
    procedure DataSetDelete1Execute(Sender: TObject);
    procedure DataSetFirst1Execute(Sender: TObject);
    procedure DataSetPrior1Execute(Sender: TObject);
    procedure DataSetNext1Execute(Sender: TObject);
    procedure DataSetLast1Execute(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Cadastro_Lideres: TF_Cadastro_Lideres;

implementation

uses U_dados;

{$R *.dfm}

procedure TF_Cadastro_Lideres.BitBtn5Click(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Pastores.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Lideres.BitBtn8Click(Sender: TObject);
begin
F_dados.Q_Pastores.Last;
end;

procedure TF_Cadastro_Lideres.BitBtn9Click(Sender: TObject);
begin
if(application.MessageBox('Tem Certeza que deseja Fechar a Janela?','Fechamento de Janela',MB_YESNO)=IDYES) THEN
Close;
end;

procedure TF_Cadastro_Lideres.FormKeyPress(Sender: TObject;
var Key: Char );
begin
  //Fechando Janela Caso ESC pressionado
  if (key=#27) then
    Begin
      close;
    End;

//Deixando Letra Grande
  Key:=UPCASE(key);


  //Trocar as letras Acentuadas por Letras sem acentos

if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';

if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';


if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';

if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';

if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';

if(key='�') OR (key='�') Then key:='C';
if(key='�') OR (key='�') THEN Key:='N';
if(key='�') OR (key='�') THEN Key:='Y';

//Ativando o ENTER, fazendo com que ao pressionar o enter ele pule de um campo para outro
if (key=#13) then
selectnext(activecontrol,true,true);
//end;
end;

procedure TF_Cadastro_Lideres.DataSetCancel1Execute(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Pastores.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End; 
end;

procedure TF_Cadastro_Lideres.DataSetPost1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
  F_dados.Q_Pastores.Post;
  F_dados.Q_Pastores.Close; //fecha tabela
  F_dados.Q_Pastores.Open; // abre tabela resultando em  Atualiza Tabela
  ShowMessage('Registro Salvo!');
 
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Lideres.DataSetInsert1Execute(Sender: TObject);
var
codGeracao:integer;
begin
//Implementacao do Novo Lider Lider ------------------------
//Inserindo Registros
//verifica se � lider, se j� for n�o vai cadastra-lo como lider, apenas criar a celula
//Se n�o dor lider ele cadastra-o como lider
if(F_dados.Q_DiscipulosE_Lider.Value = false)THEN
BEGIN
try
  Begin
    F_Dados.Q_Lideres.Insert;
  //F_Dados.Q_Liderescodlider.Value:=STRtoINT(DBText1.Caption);
  F_Dados.Q_LideresNome_Lider.Value:=DBText2.Caption;
  F_Dados.Q_Liderescoddiscipulo.Value:=STRtoINT(DBText3.Caption);
  F_Dados.Q_Lideres.Post;

  //Alterando o status de lider no discipulo(Altera o valor da select atual)
  F_dados.Q_Discipulos.Edit;
  F_Dados.Q_DiscipulosE_Lider.Value:=true;
  F_Dados.Q_DiscipulosE_CoLider.Value:=false;
  F_dados.Q_Discipulos.Post;
 End;
  Except
  showMessage('Op��o n�o Dispon�vel! Houve um erro na inse��o do lider');
  End;
END; //fim do if de

  DBGrid1.Enabled:=False;  //Desabilitando DBGrid p evitar Bug

     //Atualizando a tabela de lideres para o codigo abaixo localizar a ultima inser��o
 try
  Begin
    F_Dados.Q_Lideres.Close;
    F_Dados.Q_Lideres.Open;
  End;
  Except
    showMessage('Houve um erro na Atualiza��o de l�deres');
  End;

  ///------------------------   implementacao da celula
  // Adicionando campos de codigos
      //--indo na tabela de lideres pegar o ultimo registro de lider inserido
      //F_dados.Q_Consulta_ultimo_Lider.Close;
      //F_dados.Q_Consulta_ultimo_Lider.Open;
      //F_dados.Q_Lideres.ExecSQL;
 try
    Begin
      F_dados.Q_Consulta_ultimo_Lider.ExecSQL;
    End;
    Except
      showMessage('Houve um erro no ExecSQL ');
    End;

     F_dados.Q_Consulta_ultimo_Lider.Close;
     F_dados.Q_Consulta_ultimo_Lider.Open;
      //---
    F_dados.Q_Celulas.Insert;
    F_dados.Q_Celulascodlider.Value:=F_dados.Q_Consulta_ultimo_Lidercodlider.Value;  //codigo do lider da celula
    F_dados.Q_Celulascoddiscipulo.Value:=F_dados.Q_Discipuloscoddiscipulo.value;
    F_dados.Q_Celulascodpastor.Value:=F_dados.Q_Discipuloscodpastor.Value;
    F_dados.Q_Celulascodrede.Value:=F_dados.Q_Discipuloscodrede.Value;


    F_dados.Q_CelulasdiaSemana.Value:=ComboBox1.Text;
    F_dados.Q_CelulashorarioCelula.Value:=MaskEdit1.Text;
    F_dados.Q_Celulasendereco.Value:=Edit3.Text;
    F_dados.Q_Celulasbairro.Value:=Edit4.Text;
    F_dados.Q_Celulascidade.Value:=Edit5.Text;
    //codGeracao:=F_dados.Q_Discipuloscodgeracao.Value;
    //F_dados.Q_Celulascodgeracao.Value:=codGeracao;
  //Salvando informacoes
    F_dados.Q_Celulas.Post;

  ShowMessage('Registro Salvo!');
  ShowMessage('C�lula cadastrada com Sucesso!');

   DBGrid1.Enabled:=True; //Habilitando DBGrid Novamente
  //-----------
  //End;
  //Except
  //showMessage('Op��o n�o Dispon�vel!');
  //End;

   //Atualizando tabelas
  F_dados.Q_Lideres.Close;
  F_dados.Q_Lideres.Open;
  F_dados.Q_Celulas.Close;
  F_dados.Q_Celulas.Open;
  F_dados.Q_Discipulos.Close;
  F_dados.Q_Discipulos.Open;
//--------------------------------------------------
//--
//-- Implementacao da Celula


end;

procedure TF_Cadastro_Lideres.DataSetEdit1Execute(Sender: TObject);
begin
try //Alterando Registros
  Begin

  F_dados.Q_Lideres.Edit;
  ShowMessage('Opera��o de Edi��o Aberta!');
  End;
  Except
   ShowMessage('N�o foi poss�vel alterar os dados!');
  End;
end;

procedure TF_Cadastro_Lideres.DataSetDelete1Execute(Sender: TObject);
begin
try //Excluindo Registros
  Begin
    if(application.MessageBox('Deseja Excluir os Dados?','Confirma��o de Exclus�o',MB_YESNO)=IDYES) THEN
    Begin
        F_dados.Q_Lideres.Delete;
        ShowMessage('Dados Excluidos com Sucesso!');
    End;
  End;
  Except
   ShowMessage('N�o foi poss�vel fazer a exclus�o!');
  End;
end;

procedure TF_Cadastro_Lideres.DataSetFirst1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.First;
end;

procedure TF_Cadastro_Lideres.DataSetPrior1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Prior;
end;

procedure TF_Cadastro_Lideres.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if(key=#13)THEN
Begin
  BitBtn12.Click;
End
end;

procedure TF_Cadastro_Lideres.DataSetNext1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Next;
end;

procedure TF_Cadastro_Lideres.DataSetLast1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Last;
end;

procedure TF_Cadastro_Lideres.BitBtn11Click(Sender: TObject);
begin
//Atualiza��o de Tabela
F_Dados.Q_Discipulos.Close;
F_Dados.Q_Discipulos.Open;
end;

procedure TF_Cadastro_Lideres.FormActivate(Sender: TObject);
begin


//Atualizando Tabela ao Abrir
F_Dados.Q_Lideres.Close;
F_Dados.Q_Lideres.SQL.Clear; //limpa sql
F_dados.Q_Lideres.SQL.Add('select * from lideres'); //tras sql padr�o
F_Dados.Q_Lideres.Open;
//Alterando a Tabela Discipulos para poder pesquisar um colider para se tornar
//lider
WITH F_dados.Q_Discipulos DO
      BEGIN
        //Fechar o Q_Pastores para poder Editar as propriedades
          CLOSE;
        //Limpar s propriedade sql do Q_Pastores
          SQL.clear;
        //Escrever SQL vazio para nao mostrar resultado incorreto
          SQL.Add('SELECT * FROM discipulos where E_Lider=:C and E_Pastor=:M and E_CoLider=:P'); //and E_CoLider=:P

        //Somente os parametros de entrada
          PARAMETERS[0].Value:='false';
          PARAMETERS[1].Value:='false';
          PARAMETERS[2].Value:='true';
        //Abrindo o Q_Discipulos
        Open;
      END;
//Abrindo a Insercao de dados  em lideres e em Celulas 
F_dados.Q_Lideres.Insert;
F_dados.Q_Celulas.Insert;

end;

procedure TF_Cadastro_Lideres.BitBtn12Click(Sender: TObject);
begin
//Configura��o do Bot�o

if(RadioGroup1.itemindex=0) THEN
  Begin
    WITH F_dados.Q_Discipulos DO
      BEGIN
        //Fechar o Q_Pastores para poder Editar as propriedades
        CLOSE;
        //Como ja foi adicionar a Select no Evento OnActive, nao precisa colocar aqui
        //Limpar s propriedade sql do Q_Pastores
        SQL.clear;
        //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM discipulos where (UPPER(nome_discipulo) like UPPER(:N) and E_Lider=:C and E_Pastor=:M and E_CoLider=:P) or E_Lider=1'); //and E_CoLider=:P

        //Somente os parametros de entrada
        PARAMETERS[0].Value:='%'+Edit1.Text+'%';
        PARAMETERS[1].Value:='false';
        PARAMETERS[2].Value:='false';
        PARAMETERS[3].Value:='true';
        //abrir o q_discipulos
        Open;
        DBText4.Caption:=F_dados.Q_Discipulosnome_discipulo.Value; //adicionando o nome do discipulo ao nome do lider da celula
      END;
  END
    ELSE IF(RadioGroup1.itemindex=1) THEN
      BEGIN
        try
         Begin
            WITH F_dados.Q_Discipulos DO
              BEGIN
              //Fechar o Q_Pastores para poder Editar as propriedades
                CLOSE;
              //Limpar s propriedade sql do Q_Pastores
                SQL.clear;
              //Escrever novo comendo SQL
                SQL.Add('SELECT * FROM discipulos where (coddiscipulo = :N and E_Lider=:C and E_Pastor=:C and E_CoLider=:P) or E_lider=1');
              //passar valores para os parametros
                PARAMETERS[0].Value:=Edit1.Text;
                PARAMETERS[1].Value:='false';
                PARAMETERS[2].Value:='false';
                PARAMETERS[3].Value:='true';
              //abrir o q_discipulos
                Open;
                DBText4.Caption:=F_dados.Q_Discipulosnome_discipulo.Value; //adicionando o nome do discipulo ao nome do lider da celula

              END;
          END;
        
        Except
            ShowMessage('Erro na operac�o. Verifique os valores digitados!');
        End;
       end;
end;

procedure TF_Cadastro_Lideres.BitBtn1Click(Sender: TObject);
begin
try
  Begin
  F_dados.Q_Celulas.Insert;


  ShowMessage('Operac�o Aberta!');
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Lideres.BitBtn2Click(Sender: TObject);
begin
try
  Begin
  // Adicionando campos de codigos
      //--indo na tabela de lideres pegar o ultimo registro de lider inserido
      F_dados.Q_Lideres.Close;
      F_Dados.Q_Lideres.SQL.Clear;
      F_Dados.Q_Lideres.SQL.Add('select Max(codlider)from Lideres');
      F_dados.Q_Lideres.Open;
      F_dados.Q_Lideres.ExecSQL;
      //---
    F_dados.Q_Celulascodlider.Value:=F_dados.Q_Liderescodlider.Value;  //codigo do lider da celula
    F_dados.Q_Celulascoddiscipulo.Value:=F_dados.Q_Discipuloscoddiscipulo.value;
    F_dados.Q_Celulascodpastor.Value:=F_dados.Q_Discipuloscodpastor.Value;
    F_dados.Q_Celulascodrede.Value:=F_dados.Q_Discipuloscodrede.Value;
    F_dados.Q_Celulascodgeracao.Value:=F_dados.Q_Discipuloscodgeracao.Value;
  //Salvando informacoes
  F_dados.Q_Celulas.Post;

  DBGrid1.Enabled:=True; //Habilitando DBGrid Novamente
  ShowMessage('C�lula cadastrada com Sucesso!');
End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Lideres.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//resetando sql de Discipulos para evitar bugs
F_dados.Q_Discipulos.Close;
F_dados.Q_Discipulos.SQL.Clear;
F_dados.Q_Discipulos.SQL.Add('Select * from discipulos');
F_dados.Q_Discipulos.Open;

//resetando sql de lideres para evitar bugs
F_dados.Q_Lideres.Close;
F_Dados.Q_Lideres.SQL.Clear;
F_Dados.Q_Lideres.SQL.Add('select * from lideres');
F_dados.Q_Lideres.Open;

//implementacao para evitar bugs
F_dados.Q_Lideres.Cancel;
F_dados.Q_Celulas.Cancel;
end;

procedure TF_Cadastro_Lideres.Button1Click(Sender: TObject);
var
teste: String;
begin
ShowMessage(InttoSTR(F_dados.Q_Discipuloscodgeracao.Value));
end;

end.
