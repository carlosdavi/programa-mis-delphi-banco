object F_Relatorio_Celula: TF_Relatorio_Celula
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Relat'#243'rio de Celulas'
  ClientHeight = 532
  ClientWidth = 744
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 744
    Height = 41
    ButtonHeight = 41
    Caption = 'ToolBar1'
    TabOrder = 0
    object BitBtn3: TBitBtn
      Left = 0
      Top = 0
      Width = 105
      Height = 41
      Caption = 'Novo Relat'#243'rio'
      TabOrder = 1
      OnClick = BitBtn3Click
    end
    object BitBtn2: TBitBtn
      Left = 105
      Top = 0
      Width = 105
      Height = 41
      Caption = 'Enviar Relat'#243'rio'
      TabOrder = 0
      OnClick = BitBtn2Click
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 41
    Width = 137
    Height = 263
    Align = alLeft
    TabOrder = 1
    object RadioGroup1: TRadioGroup
      Left = 0
      Top = 6
      Width = 185
      Height = 105
      Align = alCustom
      Caption = 'Filtro de Pesquisa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      Items.Strings = (
        'Todos os L'#237'deres'
        'Nome do L'#237'der'
        'Territ'#243'rio'
        'Gera'#231#227'o')
      ParentFont = False
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 304
    Width = 744
    Height = 228
    Align = alBottom
    TabOrder = 2
    OnClick = GroupBox2Click
    object Label4: TLabel
      Left = 24
      Top = 32
      Width = 45
      Height = 16
      Caption = 'Anfitri'#227'o'
      FocusControl = DBEdit1
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 24
      Top = 80
      Width = 86
      Height = 16
      Caption = 'Endere'#231'o no dia'
      FocusControl = DBEdit2
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 328
      Top = 32
      Width = 88
      Height = 16
      Caption = 'Hor'#225'rio de In'#237'cio'
      FocusControl = DBEdit3
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 328
      Top = 80
      Width = 100
      Height = 16
      Caption = 'Hor'#225'rio de T'#233'rmino'
      FocusControl = DBEdit4
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 24
      Top = 120
      Width = 56
      Height = 16
      Caption = 'Mensagem'
      FocusControl = DBEdit5
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 448
      Top = 80
      Width = 68
      Height = 16
      Caption = 'Observa'#231#245'es'
      FocusControl = DBEdit6
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 24
      Top = 168
      Width = 88
      Height = 16
      Caption = 'Disc'#237'pulos Fixos'
      FocusControl = DBEdit7
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 164
      Top = 168
      Width = 111
      Height = 16
      Caption = 'Disc'#237'pulos Presentes'
      FocusControl = DBEdit8
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 299
      Top = 168
      Width = 52
      Height = 16
      Caption = 'Visitantes'
      FocusControl = DBEdit9
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 447
      Top = 168
      Width = 48
      Height = 16
      Caption = 'Decisoes'
      FocusControl = DBEdit10
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 592
      Top = 168
      Width = 78
      Height = 16
      Caption = 'Valor da Oferta'
      FocusControl = DBEdit11
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 447
      Top = 32
      Width = 76
      Height = 16
      Caption = 'Data da C'#233'lula'
      FocusControl = DBEdit12
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 15
      Top = 3
      Width = 69
      Height = 19
      Caption = 'Relat'#243'rio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBEdit1: TDBEdit
      Left = 24
      Top = 51
      Width = 278
      Height = 21
      DataField = 'anfitriaoCelula'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 24
      Top = 99
      Width = 278
      Height = 21
      DataField = 'enderecoCelula'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 328
      Top = 51
      Width = 105
      Height = 21
      DataField = 'horarioComeca'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 3
    end
    object DBEdit4: TDBEdit
      Left = 328
      Top = 99
      Width = 105
      Height = 21
      DataField = 'horarioTermino'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 4
    end
    object DBEdit5: TDBEdit
      Left = 24
      Top = 139
      Width = 409
      Height = 21
      DataField = 'Mensagem'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 2
    end
    object DBEdit6: TDBEdit
      Left = 448
      Top = 99
      Width = 201
      Height = 21
      DataField = 'observacoes'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 12
    end
    object DBEdit7: TDBEdit
      Left = 24
      Top = 184
      Width = 134
      Height = 21
      DataField = 'DisFixos'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 6
    end
    object DBEdit8: TDBEdit
      Left = 162
      Top = 184
      Width = 134
      Height = 21
      DataField = 'DisPresentes'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 7
    end
    object DBEdit9: TDBEdit
      Left = 299
      Top = 184
      Width = 134
      Height = 21
      DataField = 'Visitantes'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 8
    end
    object DBEdit10: TDBEdit
      Left = 447
      Top = 184
      Width = 134
      Height = 21
      DataField = 'Decisoes'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 9
    end
    object DBEdit11: TDBEdit
      Left = 592
      Top = 184
      Width = 120
      Height = 21
      DataField = 'valorOferta'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 10
    end
    object DBEdit12: TDBEdit
      Left = 447
      Top = 51
      Width = 202
      Height = 21
      DataField = 'dataCelula'
      DataSource = F_Dados.D_Rel_Celulas
      TabOrder = 5
    end
    object GroupBox4: TGroupBox
      Left = 14
      Top = 22
      Width = 185
      Height = 2
      TabOrder = 11
    end
  end
  object GroupBox3: TGroupBox
    Left = 137
    Top = 41
    Width = 607
    Height = 263
    Align = alClient
    TabOrder = 3
    object Label1: TLabel
      Left = 63
      Top = 25
      Width = 69
      Height = 20
      Caption = 'Informa'#231#227'o:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 6
      Top = 101
      Width = 107
      Height = 20
      Caption = 'Selecione o L'#237'der'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 279
      Top = 101
      Width = 113
      Height = 20
      Caption = 'Selecione a C'#233'lula'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object DBGrid1: TDBGrid
      Left = 6
      Top = 120
      Width = 251
      Height = 137
      DataSource = F_Dados.D_Discipulos
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnKeyDown = DBGrid1KeyDown
      OnKeyUp = DBGrid1KeyDown
    end
    object DBGrid2: TDBGrid
      Left = 279
      Top = 120
      Width = 274
      Height = 137
      DataSource = F_Dados.D_Celulas
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
    object Edit1: TEdit
      Left = 138
      Top = 22
      Width = 233
      Height = 28
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object BitBtn1: TBitBtn
      Left = 377
      Top = 24
      Width = 88
      Height = 28
      Caption = 'Pesquisar'
      TabOrder = 3
      OnClick = BitBtn1Click
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 658
    Top = 105
    object ListadeGeraoes1: TMenuItem
      Caption = 'Lista de Gera'#231#245'es'
      ShortCut = 16455
      Visible = False
      OnClick = ListadeGeraoes1Click
    end
  end
end
