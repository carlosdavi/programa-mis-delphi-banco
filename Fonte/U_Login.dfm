object F_Login: TF_Login
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'F_Login'
  ClientHeight = 202
  ClientWidth = 347
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 103
    Top = 15
    Width = 126
    Height = 25
    Caption = 'Acesso ao Sistema'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Rockwell Condensed'
    Font.Style = []
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 20
    Top = 52
    Width = 281
    Height = 89
    TabOrder = 0
    object Label1: TLabel
      Left = 6
      Top = 18
      Width = 111
      Height = 20
      Caption = 'Nome de Usu'#225'rio:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 40
      Top = 53
      Width = 43
      Height = 20
      Caption = 'Senha:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 122
      Top = 16
      Width = 143
      Height = 28
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object MaskEdit1: TMaskEdit
      Left = 122
      Top = 50
      Width = 143
      Height = 28
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      PasswordChar = #9689
      TabOrder = 1
      Text = ''
    end
  end
  object BitBtn1: TBitBtn
    Left = 67
    Top = 147
    Width = 89
    Height = 38
    Caption = 'Entrar'
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 171
    Top = 147
    Width = 89
    Height = 38
    Caption = 'Fechar'
    TabOrder = 2
    OnClick = BitBtn2Click
  end
end
