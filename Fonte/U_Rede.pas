unit U_Rede;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, StdCtrls, DBCtrls, Mask, ComCtrls,
  Buttons, ToolWin, DBActns, ActnList, System.Actions;

type
  TF_Cadastro_Rede = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    BitBtn6: TBitBtn;
    ToolButton2: TToolButton;
    BitBtn9: TBitBtn;
    ActionList1: TActionList;
    DataSetInsert1: TDataSetInsert;
    DataSetDelete1: TDataSetDelete;
    DataSetEdit1: TDataSetEdit;
    DataSetPost1: TDataSetPost;
    DataSetCancel1: TDataSetCancel;
    DataSetFirst1: TDataSetFirst;
    DataSetPrior1: TDataSetPrior;
    DataSetNext1: TDataSetNext;
    DataSetLast1: TDataSetLast;
    ToolButton10: TToolButton;
    BitBtn11: TBitBtn;
    GroupBox1: TGroupBox;
    ToolButton1: TToolButton;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBText1: TDBText;
    DBText2: TDBText;
    Label4: TLabel;
    BitBtn3: TBitBtn;
    ToolButton3: TToolButton;
    BitBtn4: TBitBtn;
    ToolButton4: TToolButton;
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DataSetCancel1Execute(Sender: TObject);
    procedure DataSetPost1Execute(Sender: TObject);
    procedure DataSetInsert1Execute(Sender: TObject);
    procedure DataSetEdit1Execute(Sender: TObject);
    procedure DataSetDelete1Execute(Sender: TObject);
    procedure DataSetFirst1Execute(Sender: TObject);
    procedure DataSetPrior1Execute(Sender: TObject);
    procedure DataSetNext1Execute(Sender: TObject);
    procedure DataSetLast1Execute(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    lider:integer;
    { Public declarations }
  end;

var
  F_Cadastro_Rede: TF_Cadastro_Rede;
   

implementation

uses U_dados, U_Pesquisa_Lider;

{$R *.dfm}

procedure TF_Cadastro_Rede.BitBtn5Click(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Pastores.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Rede.BitBtn8Click(Sender: TObject);
begin
F_dados.Q_Pastores.Last;
end;

procedure TF_Cadastro_Rede.BitBtn9Click(Sender: TObject);
begin
if(application.MessageBox('Tem Certeza que deseja Fechar a Janela?','Fechamento de Janela',MB_YESNO)=IDYES) THEN
Close;
end;

procedure TF_Cadastro_Rede.FormKeyPress(Sender: TObject;
var Key: Char );
begin
  //Fechando Janela Caso ESC pressionado
  if (key=#27) then
    Begin
      close;
    End;

//Deixando Letra Grande
  Key:=UPCASE(key);


  //Trocar as letras Acentuadas por Letras sem acentos

if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';

if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';


if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';

if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';

if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';

if(key='�') OR (key='�') Then key:='C';
if(key='�') OR (key='�') THEN Key:='N';
if(key='�') OR (key='�') THEN Key:='Y';

//Ativando o ENTER, fazendo com que ao pressionar o enter ele pule de um campo para outro
if (key=#13) then
selectnext(activecontrol,true,true);
//end;
end;

procedure TF_Cadastro_Rede.DataSetCancel1Execute(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Pastores.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End; 
end;

procedure TF_Cadastro_Rede.DataSetPost1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
  //F_dados.Q_Pastores.Post;
  //F_dados.Q_Pastores.Close; //fecha tabela
  //F_dados.Q_Pastores.Open; // abre tabela resultando em  Atualiza Tabela
  //ShowMessage('Registro Salvo!');
 
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Rede.DataSetInsert1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
  F_dados.Q_Rede.Insert;
  //F_Dados.Q_Liderescodlider.Value:=STRtoINT(DBText1.Caption);
  //F_Rede.Q_LideresNome_Lider.Value:=DBText2.Caption;
  //F_Dados.Q_Liderescoddiscipulo.Value:=STRtoINT(DBText3.Caption);
  //F_Dados.Q_Lideres.Post;

  //ShowMessage('Registro Salvo!');
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Rede.DataSetEdit1Execute(Sender: TObject);
begin
try //Alterando Registros
  Begin

  F_dados.Q_Rede.Edit;
  ShowMessage('Opera��o de Edi��o Aberta!');
  End;
  Except
   ShowMessage('N�o foi poss�vel alterar os dados!');
  End;
end;

procedure TF_Cadastro_Rede.DataSetDelete1Execute(Sender: TObject);
begin
try //Excluindo Registros
  Begin
    if(application.MessageBox('Deseja Excluir os Dados?','Confirma��o de Exclus�o',MB_YESNO)=IDYES) THEN
    Begin
        F_dados.Q_Rede.Delete;
        ShowMessage('Dados Excluidos com Sucesso!');
    End;
  End;
  Except
   ShowMessage('N�o foi poss�vel fazer a exclus�o!');
  End;
end;

procedure TF_Cadastro_Rede.DataSetFirst1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.First;
end;

procedure TF_Cadastro_Rede.DataSetPrior1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Prior;
end;

procedure TF_Cadastro_Rede.DataSetNext1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Next;
end;

procedure TF_Cadastro_Rede.DataSetLast1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Last;
end;

procedure TF_Cadastro_Rede.BitBtn11Click(Sender: TObject);
begin
//Atualiza��o de Tabela
F_Dados.Q_Rede.Close;
F_Dados.Q_Rede.Open;
end;

procedure TF_Cadastro_Rede.FormActivate(Sender: TObject);
begin
//Atualizando Tabela ao Abrir
F_Dados.Q_Rede.Close;
F_Dados.Q_Rede.SQL.Clear; //limpa sql
F_dados.Q_Rede.SQL.Add('select * from rede'); //tras sql padr�o
F_Dados.Q_Rede.Open;


end;

procedure TF_Cadastro_Rede.BitBtn12Click(Sender: TObject);
begin
//Configura��o do Bot�o

// if(RadioGroup1.itemindex=0) THEN
 // Begin
   // WITH F_dados.Q_Discipulos DO
     // BEGIN
        //Fechar o Q_Pastores para poder Editar as propriedades
       // CLOSE;
        //Limpar s propriedade sql do Q_Pastores
       // SQL.clear;
        //Escrever novo comendo SQL
       // SQL.Add('SELECT * FROM discipulos where UPPER(nome_discipulo) like UPPER(:N) and E_Lider=:C and E_Pastor=:M');
        //passar valores para os parametros
       // PARAMETERS[0].Value:='%'+Edit1.Text+'%';
       // PARAMETERS[1].Value:='false';
        //PARAMETERS[2].Value:='false';
        //abrir o q_pastores
       // Open;
      //END;
 // END
   // ELSE IF(RadioGroup1.itemindex=1) THEN
     // BEGIN
       // WITH F_dados.Q_Discipulos DO
         // BEGIN
          //Fechar o Q_Pastores para poder Editar as propriedades
           // CLOSE;
            //Limpar s propriedade sql do Q_Pastores
            //SQL.clear;
            //Escrever novo comendo SQL
            //SQL.Add('SELECT * FROM discipulos where coddiscipulo = :N and E_Lider=:C and E_Pastor=:C');
            //passar valores para os parametros
            //PARAMETERS[0].Value:=Edit1.Text;
            //PARAMETERS[1].Value:='false';
            //PARAMETERS[2].Value:='false';
            //abrir o q_pastores
            //Open;
          //END;
      //END;
end;

procedure TF_Cadastro_Rede.BitBtn1Click(Sender: TObject);
begin
lider:=1;
F_Consulta_Lider.ShowModal;
end;

procedure TF_Cadastro_Rede.BitBtn2Click(Sender: TObject);
begin
lider:=2;  //campo que sera consultado na tela de Consulta lideres para saber em qual campo preencher 
F_Consulta_Lider.ShowModal;
end;

procedure TF_Cadastro_Rede.BitBtn3Click(Sender: TObject);
begin

try
  Begin
    //antes de salvar ele vai inserir o nome do Lider
    F_dados.Q_RedenomeLiderRede1.Value:=DBText1.Caption; // Nome Lider 1
    F_dados.Q_RedenomeLiderRede2.Value:=DBText2.Caption; // Nome Lider 2
    F_Dados.Q_Rede.Post;
    ShowMessage('Registro Salvo com Sucesso!');
  End;
Except
    ShowMessage('N�o foi Poss�vel Salvar!');
End;
end;

procedure TF_Cadastro_Rede.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
lider:=0; //para evitar de dar bug na pesquisa de lideres(importante)
end;

procedure TF_Cadastro_Rede.BitBtn4Click(Sender: TObject);
begin
try
  Begin
    F_dados.Q_Rede.Edit;
    ShowMessage('Opera��o de edi��o ativada!');
  End;
Except
ShowMessage('N�o foi poss�vel abrir a Edi��o!');
End;
  end;

end.
