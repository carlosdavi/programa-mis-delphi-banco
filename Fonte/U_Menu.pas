unit U_Menu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,StdCtrls, Menus,
  Buttons, ComCtrls, Vcl.ToolWin;//, WinSkinData;

type
  TF_Menu = class(TForm)
    MainMenu1: TMainMenu;
    Cadastros1: TMenuItem;
    Consultas1: TMenuItem;
    Sair1: TMenuItem;
    discipulos1: TMenuItem;
    Bases1: TMenuItem;
    Pastores1: TMenuItem;
    Apostulos1: TMenuItem;
    Lideres1: TMenuItem;
    CoLideres1: TMenuItem;
    Multiplicao1: TMenuItem;
    MultiplicarClula1: TMenuItem;
    ConsultarDiscpulos1: TMenuItem;
    StatusBar1: TStatusBar;
    Rede1: TMenuItem;
    N1: TMenuItem;
    CadastroGeracoes1: TMenuItem;
    ControledeLideres1: TMenuItem;
    ToolBar1: TToolBar;
    BitBtn1: TBitBtn;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    BitBtn2: TBitBtn;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    BitBtn3: TBitBtn;
    ToolButton5: TToolButton;
    BitBtn4: TBitBtn;
    ToolBar2: TToolBar;
    Celulas1: TMenuItem;
    AlocarDiscpulo1: TMenuItem;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    Relatrios1: TMenuItem;
    EnviarRelCelula1: TMenuItem;
    N2: TMenuItem;
    Usurios1: TMenuItem;
    login1: TMenuItem;
    procedure Image1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure Bases1Click(Sender: TObject);
    procedure discipulos1Click(Sender: TObject);
    procedure Pastores1Click(Sender: TObject);
    procedure Apostulos1Click(Sender: TObject);
    procedure Lideres1Click(Sender: TObject);
    procedure CoLideres1Click(Sender: TObject);
    procedure MultiplicarClula1Click(Sender: TObject);
    procedure ConsultarDiscpulos1Click(Sender: TObject);
    procedure Rede1Click(Sender: TObject);
    procedure CadastroGeracoes1Click(Sender: TObject);
    procedure ControledeLideres1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Celulas1Click(Sender: TObject);
    procedure AlocarDiscpulo1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure EnviarRelCelula1Click(Sender: TObject);
    procedure Usurios1Click(Sender: TObject);
    procedure login1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Menu: TF_Menu;

implementation

uses U_Bases, U_Discipulos, U_Pastores, U_Apostulos, U_Lideres, U_CoLider,
  U_MultiplicacaoCelula, U_Consulta_Discipulos, U_Rede, U_Geracao,
  U_Controle_Lideres, U_Relatorio_Celula, U_Relatorio_Geral_Celula,
  U_Alocacao_Discipulo, U_CadUsuario, U_Login, U_Dados;

{$R *.dfm}

procedure TF_Menu.Image1Click(Sender: TObject);
begin
Application.Terminate;
end;

procedure TF_Menu.Sair1Click(Sender: TObject);
begin
Application.Terminate ;
end;

procedure TF_Menu.Usurios1Click(Sender: TObject);
begin
F_CadUsuario.ShowModal;
end;

procedure TF_Menu.Bases1Click(Sender: TObject);
begin
F_Cadastro_Bases.ShowModal;
end;

procedure TF_Menu.BitBtn1Click(Sender: TObject);
begin
F_Cadastro_Discipulos.ShowModal;
end;

procedure TF_Menu.BitBtn2Click(Sender: TObject);
begin
F_Cadastro_Lideres.ShowModal;
end;

procedure TF_Menu.BitBtn3Click(Sender: TObject);
begin
F_Consulta_Discipulos.ShowModal;
end;

procedure TF_Menu.BitBtn4Click(Sender: TObject);
begin
Application.Terminate;
end;

procedure TF_Menu.BitBtn5Click(Sender: TObject);
begin
F_Relatorio_Celula.ShowModal;
end;

procedure TF_Menu.BitBtn6Click(Sender: TObject);
begin
F_Relatorio_Geral_Celula.ShowModal;
end;

procedure TF_Menu.discipulos1Click(Sender: TObject);
begin
F_Cadastro_Discipulos.ShowModal;
end;

procedure TF_Menu.EnviarRelCelula1Click(Sender: TObject);
begin
F_Relatorio_Celula.ShowModal;
end;

procedure TF_Menu.FormActivate(Sender: TObject);
begin
//Implementação dos privilegios
if(F_Login.Edit1.Text='ADM') and (F_Login.MaskEdit1.Text='15841030')THEN
    Begin;
        //Resetando Sql de Usuarios oara evitar bug
  WITH F_dados.Q_Usuarios DO
        Begin
          Close;
          Sql.Clear;
          Sql.Add('Select * from Usuarios');
          Open;
        End;
      EXIT;
    End;
if(F_dados.Q_UsuariosCadDiscipulo.Value=false) THEN
    Begin
      BitBtn1.Enabled:=false;
      discipulos1.Enabled:=false;
    End;
if(F_dados.Q_UsuariosCadTerritorio.Value=false) THEN
    Begin
      Bases1.Enabled:=false;
     End;
if(F_dados.Q_UsuariosCadApostulo.Value=false) THEN
    Begin
      Apostulos1.Enabled:=false;
     End;
if(F_dados.Q_UsuariosCad12PrimeiraG.Value=false) THEN
    Begin
      Pastores1.Enabled:=false;
     End;
if(F_dados.Q_UsuariosCadRede.Value=false) THEN
    Begin
      Rede1.Enabled:=false;
     End;
if(F_dados.Q_UsuariosCadColider.Value=false) THEN
    Begin
      CoLideres1.Enabled:=false;
     End;
if(F_dados.Q_UsuariosCadLider.Value=false) THEN
    Begin
      Lideres1.Enabled:=false;
     End;
if(F_dados.Q_UsuariosCadGeracao.Value=false) THEN
    Begin
      CadastroGeracoes1.Enabled:=false;
     End;
if(F_dados.Q_Usuarioscaduser.Value=false) THEN
    Begin
      Usurios1.Enabled:=false;
      Usurios1.Caption:='';
     End;
if(F_dados.Q_UsuariosMultiplicarCelula.Value=false) THEN
    Begin
      MultiplicarClula1.Enabled:=false;
      BitBtn2.Enabled:=False;
     End;
if(F_dados.Q_UsuariosAlocarDiscipulo.Value=false) THEN
    Begin
      AlocarDiscpulo1.Enabled:=false;
    End;
if(F_dados.Q_UsuariosEnviarRelCelula.Value=false) THEN
    Begin
      EnviarRelCelula1.Enabled:=false;
      BitBtn5.Enabled:=False;
    End;
if(F_dados.Q_UsuariosConsultaDiscipulo.Value=false) THEN
    Begin
      ConsultarDiscpulos1.Enabled:=false;
      BitBtn3.Enabled:=False;
    End;
if(F_dados.Q_UsuariosControleLideres.Value=false) THEN
    Begin
      ControledeLideres1.Enabled:=false;
    End;
if(F_dados.Q_UsuariosRelatorioGerCelula.Value=false) THEN
    Begin
      Celulas1.Enabled:=false;
      BitBtn6.Enabled:=False;
    End;

    //Resetando Sql de Usuarios oara evitar bug
WITH F_dados.Q_Usuarios DO
      Begin
        Close;
        Sql.Clear;
        Sql.Add('Select * from Usuarios');
        Open;
      End;
      

end;//fim procedure

procedure TF_Menu.Pastores1Click(Sender: TObject);
begin
F_Cadastro_Pastores.ShowModal;
end;

procedure TF_Menu.AlocarDiscpulo1Click(Sender: TObject);
begin
F_Alocacao_Discipulo.ShowModal;
end;

procedure TF_Menu.Apostulos1Click(Sender: TObject);
begin
F_Cadastro_Apostulo.ShowModal;
end;

procedure TF_Menu.Lideres1Click(Sender: TObject);
begin
F_Cadastro_Lideres.ShowModal;
end;

procedure TF_Menu.login1Click(Sender: TObject);
begin
F_login.ShowModal;
end;

procedure TF_Menu.CoLideres1Click(Sender: TObject);
begin
F_Cadastro_Co_Lider.ShowModal;
end;

procedure TF_Menu.MultiplicarClula1Click(Sender: TObject);
begin
F_Multiplicacao_Celula.ShowModal;
end;

procedure TF_Menu.ConsultarDiscpulos1Click(Sender: TObject);
begin
F_Consulta_Discipulos.ShowModal;
end;

procedure TF_Menu.ControledeLideres1Click(Sender: TObject);
begin
F_Controle_Lideres.ShowModal;
end;

procedure TF_Menu.Rede1Click(Sender: TObject);
begin
F_Cadastro_Rede.ShowModal;
end;

procedure TF_Menu.CadastroGeracoes1Click(Sender: TObject);
begin
F_Cadastro_Geracao.ShowModal;
end;

procedure TF_Menu.Celulas1Click(Sender: TObject);
begin
F_Relatorio_Geral_Celula.ShowModal;

end;

end.
