unit U_Geracao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, StdCtrls, DBCtrls, Mask, ComCtrls,
  Buttons, ToolWin, DBActns, ActnList, System.Actions;

type
  TF_Cadastro_Geracao = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    BitBtn6: TBitBtn;
    ToolButton2: TToolButton;
    BitBtn9: TBitBtn;
    ActionList1: TActionList;
    DataSetInsert1: TDataSetInsert;
    DataSetDelete1: TDataSetDelete;
    DataSetEdit1: TDataSetEdit;
    DataSetPost1: TDataSetPost;
    DataSetCancel1: TDataSetCancel;
    DataSetFirst1: TDataSetFirst;
    DataSetPrior1: TDataSetPrior;
    DataSetNext1: TDataSetNext;
    DataSetLast1: TDataSetLast;
    ToolButton10: TToolButton;
    BitBtn11: TBitBtn;
    GroupBox1: TGroupBox;
    ToolButton1: TToolButton;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    BitBtn1: TBitBtn;
    DBText1: TDBText;
    DBText2: TDBText;
    Label6: TLabel;
    Label7: TLabel;
    DBText3: TDBText;
    DBText4: TDBText;
    BitBtn2: TBitBtn;
    DBText5: TDBText;
    Label8: TLabel;
    DBText6: TDBText;
    BitBtn3: TBitBtn;
    Label10: TLabel;
    DBText8: TDBText;
    DBText9: TDBText;
    BitBtn5: TBitBtn;
    BitBtn7: TBitBtn;
    ToolButton3: TToolButton;
    DBEdit2: TDBEdit;
    DBText7: TDBText;
    DBText10: TDBText;
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DataSetCancel1Execute(Sender: TObject);
    procedure DataSetPost1Execute(Sender: TObject);
    procedure DataSetInsert1Execute(Sender: TObject);
    procedure DataSetEdit1Execute(Sender: TObject);
    procedure DataSetDelete1Execute(Sender: TObject);
    procedure DataSetFirst1Execute(Sender: TObject);
    procedure DataSetPrior1Execute(Sender: TObject);
    procedure DataSetNext1Execute(Sender: TObject);
    procedure DataSetLast1Execute(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Cadastro_Geracao: TF_Cadastro_Geracao;

implementation

uses U_dados, U_Pesquisa_Lider, U_Pesquisa_Pastor, U_Selecao_de_Rede;

{$R *.dfm}

procedure TF_Cadastro_Geracao.BitBtn5Click(Sender: TObject);
begin
F_Consultar_Rede.codAcesso:=7;
F_Consultar_Rede.ShowModal;

end;

procedure TF_Cadastro_Geracao.BitBtn8Click(Sender: TObject);
begin
F_dados.Q_Pastores.Last;
end;

procedure TF_Cadastro_Geracao.BitBtn9Click(Sender: TObject);
begin
if(application.MessageBox('Tem Certeza que deseja Fechar a Janela?','Fechamento de Janela',MB_YESNO)=IDYES) THEN
Close;
end;

procedure TF_Cadastro_Geracao.FormKeyPress(Sender: TObject;
var Key: Char );
begin
  //Fechando Janela Caso ESC pressionado
  if (key=#27) then
    Begin
      close;
    End;

//Deixando Letra Grande
  Key:=UPCASE(key);


  //Trocar as letras Acentuadas por Letras sem acentos

if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';
if(key='�') OR (key='�') Then key:='A';

if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';
if(key='�') OR (key='�') Then key:='E';


if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';
if(key='�') OR (key='�') Then key:='I';

if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';
if(key='�') OR (key='�') Then key:='O';

if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';
if(key='�') OR (key='�') Then key:='U';

if(key='�') OR (key='�') Then key:='C';
if(key='�') OR (key='�') THEN Key:='N';
if(key='�') OR (key='�') THEN Key:='Y';

//Ativando o ENTER, fazendo com que ao pressionar o enter ele pule de um campo para outro
if (key=#13) then
selectnext(activecontrol,true,true);
//end;
end;

procedure TF_Cadastro_Geracao.DataSetCancel1Execute(Sender: TObject);
begin
if (application.MessageBox('Confirmar Cancelamento?','Cancelar Registro',MB_YESNO)=IDYES) Then
try   //Excluindo REGISTROS
  Begin

    F_dados.Q_Pastores.cancel;
    ShowMessage('Opera��o Cancelada!');
  End;
  Except
  ShowMessage('Opera��o N�o Dispon�vel!');
  End; 
end;

procedure TF_Cadastro_Geracao.DataSetPost1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
  F_dados.Q_Pastores.Post;
  F_dados.Q_Pastores.Close; //fecha tabela
  F_dados.Q_Pastores.Open; // abre tabela resultando em  Atualiza Tabela
  ShowMessage('Registro Salvo!');
 
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Geracao.DataSetInsert1Execute(Sender: TObject);
begin
//Inserindo Registros
try
  Begin
  F_dados.Q_Geracoes.Insert;
  //F_Dados.Q_Liderescodlider.Value:=STRtoINT(DBText1.Caption);
  //F_Dados.Q_LideresNome_Lider.Value:=DBText2.Caption;
  //F_Dados.Q_Liderescoddiscipulo.Value:=STRtoINT(DBText3.Caption);
  //F_Dados.Q_Lideres.Post;

  //ShowMessage('Registro Salvo!');
  End;
  Except
  showMessage('Op��o n�o Dispon�vel!');
  End;
end;

procedure TF_Cadastro_Geracao.DataSetEdit1Execute(Sender: TObject);
begin
try //Alterando Registros
  Begin

  F_dados.Q_Lideres.Edit;
  ShowMessage('Opera��o de Edi��o Aberta!');
  End;
  Except
   ShowMessage('N�o foi poss�vel alterar os dados!');
  End;
end;

procedure TF_Cadastro_Geracao.DataSetDelete1Execute(Sender: TObject);
begin
try //Excluindo Registros
  Begin
    if(application.MessageBox('Deseja Excluir os Dados?','Confirma��o de Exclus�o',MB_YESNO)=IDYES) THEN
    Begin
        F_dados.Q_Lideres.Delete;
        ShowMessage('Dados Excluidos com Sucesso!');
    End;
  End;
  Except
   ShowMessage('N�o foi poss�vel fazer a exclus�o!');
  End;
end;

procedure TF_Cadastro_Geracao.DataSetFirst1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.First;
end;

procedure TF_Cadastro_Geracao.DataSetPrior1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Prior;
end;

procedure TF_Cadastro_Geracao.DataSetNext1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Next;
end;

procedure TF_Cadastro_Geracao.DataSetLast1Execute(Sender: TObject);
begin
F_dados.Q_Lideres.Last;
end;

procedure TF_Cadastro_Geracao.BitBtn11Click(Sender: TObject);
begin
//Atualiza��o de Tabela
F_Dados.Q_Lideres.Close;
F_Dados.Q_Lideres.Open;
end;

procedure TF_Cadastro_Geracao.FormActivate(Sender: TObject);
begin
//Atualizando Tabela ao Abrir
F_Dados.Q_Lideres.Close;
F_Dados.Q_Lideres.SQL.Clear; //limpa sql
F_dados.Q_Lideres.SQL.Add('select * from lideres'); //tras sql padr�o
F_Dados.Q_Lideres.Open;


end;

procedure TF_Cadastro_Geracao.BitBtn1Click(Sender: TObject);
begin
F_Consulta_Lider.codAcesso:=3;
F_Consulta_Lider.ShowModal;
end;

procedure TF_Cadastro_Geracao.BitBtn2Click(Sender: TObject);
begin
F_Consulta_Lider.codAcesso:=4;
F_Consulta_Lider.ShowModal;
end;

procedure TF_Cadastro_Geracao.BitBtn3Click(Sender: TObject);
begin
F_Consulta_Pastor.codAcesso:=5;
F_Consulta_Pastor.ShowModal;
end;

procedure TF_Cadastro_Geracao.BitBtn4Click(Sender: TObject);
begin
F_Consultar_Rede.codAcesso:=2;
F_Consultar_Rede.ShowModal;
end;

procedure TF_Cadastro_Geracao.BitBtn7Click(Sender: TObject);
var
codGeracao:integer;
codDiscipulo:integer;
codDiscipulo2:integer;
begin
//try
  //Begin
    //setando valores
    F_dados.Q_Geracoescodrede.Value:=STRTOINT(DBText8.Caption); //codred
    F_dados.Q_Geracoescodpastor.Value:=STRTOINT(DBText6.Caption);//codpastor
    F_dados.Q_Geracoescodlider.Value:=STRTOINT(DBText3.Caption);//codlider
    F_dados.Q_Geracoescodlider2.Value:=STRTOINT(DBText4.Caption);//codlider2
    F_dados.Q_GeracoesnomeLider2.Value:=DBText2.Caption;//nome lider2
    F_dados.Q_Geracoescoddiscipulo1.Value:= STRTOINT(DBText7.Caption);//coddiscipulo
    F_dados.Q_Geracoescoddiscipulo2.Value:= STRTOINT(DBText10.Caption);//coddiscipulo2
    //implementacao da correcao do bug do lider da geracao q ficava sem geracao
       codDiscipulo:=STRTOINT(DBText7.Caption);
       codDiscipulo2:=STRTOINT(DBText10.Caption);
  try
      Begin
        F_Dados.Q_Geracoes.Post;

         //implementacao da correcao do bug do lider da geracao q ficava sem geracao
   WITH F_Dados.Q_Geracoes DO
      Begin
          close;
          sql.Clear;
          sql.Add('select * from geracoes order by codgeracao DESC');
          open;
      End;
      codGeracao:=F_Dados.Q_Geracoescodgeracao.Value;
          ShowMessage(''+inttostr(codGeracao));
   //Agora Pesquisando o lider para alterar o campo cod geracao(em discipulos)
      WITH F_Dados.Q_Discipulos DO
      Begin
          close;
          sql.Clear;
          sql.Add('select * from discipulos where coddiscipulo=:c');
          Parameters[0].Value:=codDiscipulo;
          open;
      End;
      //Agora que ele ja achou o lider vamos editar o campo
          F_Dados.Q_Discipulos.Edit; //habilita edi��o
          F_Dados.Q_Discipuloscodgeracao.Value:=codGeracao;
          F_Dados.Q_Discipulos.Post; //Salva altera��es

      //Agora Pesquisando o lider 2 para alterar o campo cod geracao(em discipulos)
      WITH F_Dados.Q_Discipulos DO
      Begin
          close;
          sql.Clear;
          sql.Add('select * from discipulos where coddiscipulo=:c');
          Parameters[0].Value:=codDiscipulo2;
          open;
       End;
         //Agora que ele ja achou o lider vamos editar o campo
          F_Dados.Q_Discipulos.Edit; //habilita edi��o
          F_Dados.Q_Discipuloscodgeracao.Value:=codGeracao;
          F_Dados.Q_Discipulos.Post; //Salva altera��es

        ShowMessage('Registro Salvo!');
      End;
    Except
      Begin
          ShowMessage('N�o foi poss�vel salvar!');
      End;


  end;
  End;
 // Except
   // showMessage('Op��o n�o Dispon�vel!');
  //End;
//end;

end.
