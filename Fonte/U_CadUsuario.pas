unit U_CadUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ToolWin, Vcl.ComCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons, Vcl.DBCtrls, Vcl.Mask;

type
  TF_CadUsuario = class(TForm)
    Panel1: TPanel;
    ToolBar1: TToolBar;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBGrid1: TDBGrid;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    DBCheckBox8: TDBCheckBox;
    DBCheckBox9: TDBCheckBox;
    DBCheckBox10: TDBCheckBox;
    DBCheckBox11: TDBCheckBox;
    DBCheckBox12: TDBCheckBox;
    DBCheckBox13: TDBCheckBox;
    DBCheckBox14: TDBCheckBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadUsuario: TF_CadUsuario;

implementation

{$R *.dfm}

uses U_Dados;

procedure TF_CadUsuario.BitBtn1Click(Sender: TObject);
begin
try
   Begin
     F_dados.Q_Usuarios.Insert;
     ShowMessage('Opera��o aberta!');
   End;
except
ShowMessage('N�o foi poss�vel realizar a opera��o!');
end;
end;

procedure TF_CadUsuario.BitBtn2Click(Sender: TObject);
begin
try
    Begin
    F_dados.Q_Usuarios.Post;
    ShowMessage('Dados salvos!');
    End;
except
    ShowMessage('N�o foi poss�vel realizar a opera��o!');
end;
end;

procedure TF_CadUsuario.BitBtn3Click(Sender: TObject);
begin
try
    Begin
      F_dados.Q_Usuarios.Edit;
      ShowMessage('Edi��o aberta!');
    End;
except
      ShowMessage('N�o foi poss�vel realizar a opera��o!');
end;
end;

procedure TF_CadUsuario.BitBtn4Click(Sender: TObject);
begin
try
   Begin
     F_Dados.Q_Usuarios.Cancel;
     ShowMessage('Opera��o cancelada!');
   End;
except
ShowMessage('N�o foi poss�vel cancelar!');
end;
end;

procedure TF_CadUsuario.BitBtn5Click(Sender: TObject);
begin
F_CadUsuario.Close;
end;

end.
