unit U_Alocacao_Discipulo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ToolWin, Vcl.ComCtrls, Vcl.ExtCtrls,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.Buttons, Vcl.Mask;

type
  TF_Alocacao_Discipulo = class(TForm)
    ToolBar1: TToolBar;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    MaskEdit1: TMaskEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn2: TBitBtn;
    Label4: TLabel;
    procedure RadioGroup1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    codLider:Integer;
  end;

var
  F_Alocacao_Discipulo: TF_Alocacao_Discipulo;

implementation

{$R *.dfm}

uses U_Discipulos, U_Dados;

procedure TF_Alocacao_Discipulo.BitBtn1Click(Sender: TObject);
begin
//aqui ele vai realizar a pesquisa do discipulo (TODOS)
if (RadioGroup1.ItemIndex=0) then
  Begin
    WITH F_dados.Q_Discipulos DO
      Begin
        close;
        sql.Clear;
        sql.Add('select * from discipulos where codcelula IS NULL');
        Open;
      End;
  End;
//implementa��o para sele��o de discipulo pelo nome
if (RadioGroup1.ItemIndex=1) then
  Begin

    WITH F_dados.Q_Discipulos DO
      Begin
        close;
        sql.Clear;
        sql.Add('select * from discipulos where codcelula IS NULL and nome_discipulo like :n');
        Parameters[0].Value:='%'+MaskEdit1.Text+'%';
        Open;
      End;

  End;
//implementa��o para selecao de discipulo pela data de aceitacao
if (RadioGroup1.ItemIndex=2) then
  Begin
     WITH F_dados.Q_Discipulos DO
      Begin
        close;
        sql.Clear;
        sql.Add('select * from discipulos where codcelula IS NULL and data_aceitacao=:n');
        Parameters[0].Value:=MaskEdit1.Text;
        Open;
      End;
  End;
end;

procedure TF_Alocacao_Discipulo.BitBtn2Click(Sender: TObject);
begin
//esse codigo editar o Discipulo atual no campo cod celula
//passando nesse minuto a participar de uma celula
try
   WITH F_dados.Q_Discipulos DO
      Begin
        Edit; //Abre edi��o
        F_Dados.Q_Discipuloscodcelula.Value:=F_dados.Q_Celulascodcelula.Value;
        Post; //salva
        ShowMessage('Discipulo Alocado com Sucesso!');
      End;
except on E: Exception do
  Begin
    ShowMessage('N�o foi possivel alocar disc�pulo!')
  End;
end;
end;

procedure TF_Alocacao_Discipulo.DBGrid1CellClick(Column: TColumn);
begin

//Pesquisando as celulas que o discipulo pode ficar
WITH F_dados.Q_Celulas DO
  begin
    close;
    sql.Clear;
    sql.Add('select * from celulas where codlider=:c');
    codLider:=F_dados.Q_Discipuloscodlider.Value;
    Parameters[0].Value:=codLider;
    Open;
  end;
end;

procedure TF_Alocacao_Discipulo.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//Pesquisando as celulas que o discipulo pode ficar
WITH F_dados.Q_Celulas DO
  begin
    close;
    sql.Clear;
    sql.Add('select * from celulas where codlider=:c');
    codLider:=F_dados.Q_Discipuloscodlider.Value;
    Parameters[0].Value:=codLider;
    Open;
  end;
end;

procedure TF_Alocacao_Discipulo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//Implementa��o para evitar bugs nas tabelas abaixo
//resetando discipulos
WITH F_dados.Q_Discipulos DO
  Begin
    close;
    sql.clear;
    sql.Add('select * from discipulos');
    open;
  End;
WITH F_dados.Q_Celulas DO
  Begin
    close;
    sql.clear;
    sql.Add('select * from celulas');
    open;
  End;
end;//fim procedure

procedure TF_Alocacao_Discipulo.RadioGroup1Click(Sender: TObject);
begin
if (RadioGroup1.ItemIndex=0) then
  Begin
    MaskEdit1.Text:=''; //resetando texto
    MaskEdit1.EditMask:=''; //resetando formato
  End;
if (RadioGroup1.ItemIndex=1) then
  Begin
    MaskEdit1.Text:=''; //resetando texto
    MaskEdit1.EditMask:=''; //resetando formato
  End;
if (RadioGroup1.ItemIndex=2) then
  Begin
    MaskEdit1.Text:=''; //resetando texto
    MaskEdit1.EditMask:='99/99/9999'; //resetando formato
  End;
end;

end.
