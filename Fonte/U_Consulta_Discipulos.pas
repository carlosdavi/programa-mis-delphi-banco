unit U_Consulta_Discipulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, ExtCtrls, Mask,ComObj,
  DBCtrls, Vcl.Menus; //ComObj eu q coloquei

type
  TF_Consulta_Discipulos = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    RadioGroup1: TRadioGroup;
    BitBtn3: TBitBtn;
    Panel1: TPanel;
    MaskEdit1: TMaskEdit;
    DBGrid1: TDBGrid;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    DBText1: TDBText;
    Label4: TLabel;
    DBText2: TDBText;
    Label20: TLabel;
    DBText3: TDBText;
    DBText4: TDBText;
    Label10: TLabel;
    Label6: TLabel;
    DBText6: TDBText;
    Label9: TLabel;
    DBText7: TDBText;
    Label8: TLabel;
    DBText8: TDBText;
    Label14: TLabel;
    DBText9: TDBText;
    Label7: TLabel;
    DBText10: TDBText;
    Label19: TLabel;
    GroupBox4: TGroupBox;
    Label21: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label15: TLabel;
    DBText12: TDBText;
    DBText13: TDBText;
    DBText14: TDBText;
    DBText15: TDBText;
    DBText16: TDBText;
    DBText17: TDBText;
    Label16: TLabel;
    DBText11: TDBText;
    DBText19: TDBText;
    DBMemo1: TDBMemo;
    Label5: TLabel;
    Label22: TLabel;
    DBText5: TDBText;
    Label23: TLabel;
    DBText18: TDBText;
    PopupMenu1: TPopupMenu;
    ListadeGeracoes1: TMenuItem;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure MaskEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure RadioGroup1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure ListadeGeracoes1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Consulta_Discipulos: TF_Consulta_Discipulos;

implementation

uses U_dados, ADODB, U_Lista_Geracoes;

{$R *.dfm}

procedure TF_Consulta_Discipulos.BitBtn2Click(Sender: TObject);
begin
CLOSE;
end;

procedure TF_Consulta_Discipulos.BitBtn1Click(Sender: TObject);
var
    codigoGer:Integer;
begin
//Configura��o do Bot�o
if(RadioGroup1.ItemIndex=0) THEN    //PARA TODOS
  Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio');
      //abrir o q_Turmas
        Open;
      END;
  End;
if(RadioGroup1.ItemIndex=1) THEN   //PARA NOME
Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where nome_discipulo like :N');
        Parameters[0].Value:='%'+MaskEdit1.Text+'%';
      //abrir o q_Turmas
        Open;
      END;
  End;
if(RadioGroup1.ItemIndex=2) THEN   //TODOS OS DISCIPULOS PELO LIDER
Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where Nome_Lider like :N');
        Parameters[0].Value:='%'+MaskEdit1.Text+'%';
      //abrir o q_Turmas
        Open;
      END;
  End;
if(RadioGroup1.ItemIndex=3) THEN   //TODOS OS DISCIPULOS PELO PASTOR
Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where nome_pastor like :N');
        Parameters[0].Value:='%'+MaskEdit1.Text+'%';
      //abrir o q_Turmas
        Open;
      END;
  End;
if(RadioGroup1.ItemIndex=4) THEN //PARA  NOME DA BASE
    Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where nome_base like :N');
        Parameters[0].Value:='%'+MaskEdit1.Text+'%';
      //abrir o q_Turmas
        Open;
      END;
  End;

  if(RadioGroup1.ItemIndex=5) THEN //PARA O SEXO
  Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where sexo=:S');
        Parameters[0].Value:=MaskEdit1.Text;
      //abrir o q_Turmas
        Open;
      END;
  End;

  if(RadioGroup1.ItemIndex=6) THEN //PARA  DATA NASCIMENTO
    Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where CONVERT(VARCHAR,nasc_discipulo,3)=:N');
        Parameters[0].Value:=MaskEdit1.Text;
      //abrir o q_Turmas
        Open;
      END;
  End;
if(RadioGroup1.ItemIndex=7) THEN //PARA  DATA CADASTRO
    Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where CONVERT(VARCHAR,datacad,3)=:N');
        Parameters[0].Value:=MaskEdit1.Text;
      //abrir o q_Turmas
        Open;
      END;
  End;
if(RadioGroup1.ItemIndex=8) THEN //PARA  DATA ACEITACAO
    Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where CONVERT(VARCHAR,data_aceitacao,3)=:N');
        Parameters[0].Value:=MaskEdit1.Text;
      //abrir o q_Turmas
        Open;
      END;
  End;


if(RadioGroup1.ItemIndex=9) THEN //PARA  PAra BATIZADOS
    Begin
    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where batismo_discipulo like :N');
        Parameters[0].Value:='%'+MaskEdit1.Text+'%';
      //abrir o q_Turmas
        Open;
      END;
  End;

if(RadioGroup1.ItemIndex=10) THEN //tODOS OS DISCIPULOS POR GERA��O

    Begin
    //FUN��O INTERMEDIARIA QUE PESQUISA O CODGERA��O
    try
      Begin
        F_dados.Q_Geracoes.Close;
        F_dados.Q_Geracoes.SQL.Clear;
        F_dados.Q_Geracoes.SQL.Add('Select * from geracoes where nomeGeracao like :C');
        F_dados.Q_Geracoes.Parameters[0].Value:='%'+MaskEdit1.Text+'%';
        F_dados.Q_Geracoes.Open;
        F_dados.Q_Geracoes.ExecSQL;
      End;
    Except
        ShowMessage('Houve erro ao pesquisar codGer');
    end;
    //Aqui eu pego o codigo e coloco em uma variavel auxiliar
    codigoGer:=F_dados.Q_Geracoescodgeracao.Value;

    //Retornando ao que era antes
    F_dados.Q_Geracoes.Close;
    F_dados.Q_Geracoes.SQL.Clear;
    F_dados.Q_Geracoes.SQL.Add('Select * from geracoes');
    F_dados.Q_Geracoes.Open;


    WITH F_dados.Q_Visao_Rel_DiscipuloLider DO
      BEGIN
      //Fechar o Q_Discipulos para poder Editar as propriedades
        CLOSE;
      //Limpar s propriedade sql do Q_Turmas
        SQL.clear;
      //Escrever novo comendo SQL
        SQL.Add('SELECT * FROM Visao_DiscipuloeLiderPastorRelatorio where codgeracao=:N');
        Parameters[0].Value:=codigoGer;
      //abrir o q_Turmas
        Open;
      END;
  End;

end; //Fim M�todo

procedure TF_Consulta_Discipulos.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
//configurando para executar ao apertar enter

    if(KEY=#13) THEN
    Begin
    BitBtn1.Click; // vai dar um clique no Bot�o
    End;
end;

procedure TF_Consulta_Discipulos.ListadeGeracoes1Click(Sender: TObject);
begin
F_Lista_Geracoes.codAcesso:=1;
F_Lista_Geracoes.ShowModal;
end;

procedure TF_Consulta_Discipulos.MaskEdit1KeyPress(Sender: TObject;
  var Key: Char);
begin
 //configurando para executar ao apertar enter

    if(KEY=#13) THEN
    Begin
    BitBtn1.Click; // vai dar um clique no Bot�o
    End;
end;

procedure TF_Consulta_Discipulos.RadioGroup1Click(Sender: TObject);
begin
//Condi��o para alterar a formaa��o do texto em data

case(RadioGroup1.ItemIndex)of
  5:
    begin
      MaskEdit1.Text:='';
      MaskEdit1.EditMask:='L';
    end;
  6:
    begin
      MaskEdit1.Text:='';
      MaskEdit1.EditMask:='99/99/9999';
    end;
  7:
    begin
      MaskEdit1.Text:='';
      MaskEdit1.EditMask:='99/99/9999';
    end;
  8:
    begin
      MaskEdit1.Text:='';
      MaskEdit1.EditMask:='99/99/9999';
    end;

    9:
    begin
      MaskEdit1.Text:='';
      MaskEdit1.EditMask:='LLL';
    end;
  else
    begin
      MaskEdit1.Text:='';
      MaskEdit1.EditMask:='';
    end;
end; //Fim case

end;

procedure TF_Consulta_Discipulos.BitBtn3Click(Sender: TObject);

var linha, coluna : integer;
var planilha : variant;
var valorcampo : string;
begin
//F_dados.Q_Visao_Rel_DiscipuloLider.CachedUpdates := true;
 planilha:= CreateoleObject('Excel.Application');
 planilha.WorkBooks.add(1);
 planilha.caption := 'Exportando dados do dbGrid para o Excel';
 planilha.visible := true;

 //F_dados.Q_Visao_Rel_DiscipuloLider.ApplyUpdates;
 F_dados.Q_Visao_Rel_DiscipuloLider.First;
 for linha := 0 to F_dados.Q_Visao_Rel_DiscipuloLider.RecordCount - 1 do
 begin
   for coluna := 1 to F_dados.Q_Visao_Rel_DiscipuloLider.FieldCount do
   begin
     valorcampo := F_dados.Q_Visao_Rel_DiscipuloLider.Fields[coluna - 1].AsString;
     planilha.cells[linha + 2,coluna] := valorCampo;
   end;
   F_dados.Q_Visao_Rel_DiscipuloLider.Next;
 end;
 for coluna := 1 to F_dados.Q_Visao_Rel_DiscipuloLider.FieldCount do
 begin
   valorcampo := F_dados.Q_Visao_Rel_DiscipuloLider.Fields[coluna - 1].DisplayLabel;
   planilha.cells[1,coluna] := valorcampo;
 end;
 planilha.columns.Autofit;
end;

end.
