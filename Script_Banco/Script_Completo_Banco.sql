USE [dados_mis]
GO
/****** Object:  Table [dbo].[Bases]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bases](
	[codbase] [int] IDENTITY(1,1) NOT NULL,
	[nome_base] [varchar](30) NOT NULL,
	[nome_responsavel] [varchar](30) NULL,
	[ender_base] [varchar](30) NULL,
	[fone1_base] [varchar](15) NULL,
	[fone2_base] [varchar](15) NULL,
	[datacad] [datetime] NULL,
 CONSTRAINT [PK__Bases__7C8480AE] PRIMARY KEY CLUSTERED 
(
	[codbase] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pastores]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pastores](
	[codpastor] [int] IDENTITY(1,1) NOT NULL,
	[nome_pastor] [varchar](50) NOT NULL,
	[codbase] [int] NULL,
	[coddiscipulo] [int] NULL,
	[datacad] [datetime] NULL,
 CONSTRAINT [PK__pastores__7C8480AE] PRIMARY KEY CLUSTERED 
(
	[codpastor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lideres]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lideres](
	[codlider] [int] IDENTITY(1,1) NOT NULL,
	[Nome_Lider] [varchar](50) NOT NULL,
	[coddiscipulo] [int] NULL,
	[datacad] [datetime] NULL,
 CONSTRAINT [PK_Lideres] PRIMARY KEY CLUSTERED 
(
	[codlider] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rede]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rede](
	[codrede] [int] IDENTITY(1,1) NOT NULL,
	[nomeRede] [varchar](30) NOT NULL,
	[nomeLiderRede1] [varchar](30) NULL,
	[nomeLiderRede2] [varchar](30) NULL,
	[coddiscipulo1] [int] NULL,
	[codlider1] [int] NULL,
	[coddiscipulo2] [int] NULL,
	[codlider2] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[codrede] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[geracoes]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[geracoes](
	[codgeracao] [int] IDENTITY(1,1) NOT NULL,
	[nomeGeracao] [varchar](50) NOT NULL,
	[codrede] [int] NOT NULL,
	[codpastor] [int] NULL,
	[codlider] [int] NOT NULL,
	[codlider2] [int] NULL,
	[coddiscipulo1] [int] NULL,
	[coddiscipulo2] [int] NULL,
	[nomeLider2] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[codgeracao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[discipulos]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[discipulos](
	[coddiscipulo] [int] IDENTITY(1,1) NOT NULL,
	[nome_discipulo] [varchar](50) NOT NULL,
	[data_aceitacao] [varchar](10) NOT NULL,
	[estado_civil_discipulo] [varchar](10) NULL,
	[Trabalho] [varchar](50) NULL,
	[motivo_oracao] [ntext] NULL,
	[melhor_horario] [varchar](50) NULL,
	[ender_discipulo] [varchar](30) NOT NULL,
	[bairro_discipulo] [varchar](10) NOT NULL,
	[cidade_discipulo] [varchar](10) NOT NULL,
	[estado_discipulo] [varchar](2) NOT NULL,
	[nasc_discipulo] [varchar](10) NULL,
	[fone_discipulo] [varchar](15) NULL,
	[fone2_discipulo] [varchar](15) NULL,
	[sexo] [char](1) NULL,
	[modulo_discipulo] [varchar](30) NULL,
	[situacao_discipulo] [varchar](30) NULL,
	[batismo_discipulo] [varchar](3) NULL,
	[data_batismo] [datetime] NULL,
	[codpastor] [int] NULL,
	[tornar_lider] [varchar](3) NULL,
	[codbase] [int] NOT NULL,
	[data_cad] [datetime] NULL,
	[E_Lider] [bit] NULL,
	[E_Pastor] [bit] NULL,
	[E_CoLider] [bit] NULL,
	[E_Lider_Geracao] [bit] NULL,
	[codlider] [int] NULL,
	[datacad] [datetime] NULL,
	[redeDiscipulo] [nchar](20) NULL,
	[codgeracao] [int] NULL,
	[codrede] [int] NULL,
	[codcelula] [int] NULL,
 CONSTRAINT [PK__discipulos__0425A276] PRIMARY KEY CLUSTERED 
(
	[coddiscipulo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[sp_Multiplicar_Discipulo]    Script Date: 04/15/2015 11:22:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_Multiplicar_Discipulo]
@Cod_LiderVelho int,@Cod_LiderNovo int,@Cod_Discipulo int
as
update discipulos set codlider=@Cod_LiderNovo where coddiscipulo=@Cod_Discipulo and codlider=@Cod_LiderVelho
GO
/****** Object:  StoredProcedure [dbo].[sp_inserir_CoLider]    Script Date: 04/15/2015 11:22:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_inserir_CoLider]
@Cod_Discipulo int
as
update discipulos set E_CoLider=1 where coddiscipulo=@Cod_Discipulo
GO
/****** Object:  Table [dbo].[celulas]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[celulas](
	[codcelula] [int] IDENTITY(1,1) NOT NULL,
	[codlider] [int] NOT NULL,
	[coddiscipulo] [int] NOT NULL,
	[codpastor] [int] NOT NULL,
	[codrede] [int] NOT NULL,
	[diaSemana] [varchar](50) NULL,
	[horarioCelula] [varchar](50) NULL,
	[codgeracao] [int] NULL,
	[endereco] [varchar](50) NULL,
	[bairro] [varchar](50) NULL,
	[cidade] [nchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[codcelula] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[relatorios_celulas]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[relatorios_celulas](
	[codRelCelula] [int] IDENTITY(1,1) NOT NULL,
	[codcelula] [int] NOT NULL,
	[anfitriaoCelula] [varchar](50) NULL,
	[enderecoCelula] [varchar](60) NULL,
	[horarioComeca] [varchar](50) NULL,
	[horarioTermino] [varchar](50) NULL,
	[Mensagem] [varchar](70) NULL,
	[observacoes] [varchar](50) NULL,
	[DisFixos] [int] NULL,
	[DisPresentes] [int] NULL,
	[Visitantes] [int] NULL,
	[Decisoes] [int] NULL,
	[valorOferta] [money] NULL,
	[dataCelula] [varchar](20) NULL,
	[dataRegistro] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[codRelCelula] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[Visao_DiscipulosPorCelula]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Visao_DiscipulosPorCelula]
AS
SELECT     coddiscipulo, nome_discipulo, data_aceitacao, estado_civil_discipulo, Trabalho, motivo_oracao, melhor_horario, ender_discipulo, bairro_discipulo, cidade_discipulo, estado_discipulo, 
                      nasc_discipulo, fone_discipulo, fone2_discipulo, sexo, modulo_discipulo, situacao_discipulo, batismo_discipulo, data_batismo, codpastor, tornar_lider, codbase, data_cad, E_Lider, E_Pastor, 
                      E_CoLider, E_Lider_Geracao, codlider, datacad, redeDiscipulo, codgeracao, codrede, codcelula
FROM         dbo.discipulos
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[26] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "discipulos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 206
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 24
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_DiscipulosPorCelula'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_DiscipulosPorCelula'
GO
/****** Object:  View [dbo].[Visao_Discipulo_pMultiplicacao]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Visao_Discipulo_pMultiplicacao] as
select Discipulos.nome_discipulo,Discipulos.coddiscipulo,Discipulos.codlider
from Discipulos
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[codusuario] [int] IDENTITY(1,1) NOT NULL,
	[Nome_user] [varchar](20) NOT NULL,
	[Senha] [varchar](15) NOT NULL,
	[CadDiscipulo] [bit] NULL,
	[CadTerritorio] [bit] NULL,
	[CadApostulo] [bit] NULL,
	[Cad12PrimeiraG] [bit] NULL,
	[CadRede] [bit] NULL,
	[CadColider] [bit] NULL,
	[CadLider] [bit] NULL,
	[CadGeracao] [bit] NULL,
	[MultiplicarCelula] [bit] NULL,
	[AlocarDiscipulo] [bit] NULL,
	[EnviarRelCelula] [bit] NULL,
	[ConsultaDiscipulo] [bit] NULL,
	[ControleLideres] [bit] NULL,
	[RelatorioGerCelula] [bit] NULL,
	[datacad] [date] NULL,
	[caduser] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[codusuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[Visao_Inserir_Rel_Celula]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Visao_Inserir_Rel_Celula]
AS
SELECT     dbo.relatorios_celulas.codRelCelula, dbo.celulas.codcelula, dbo.relatorios_celulas.anfitriaoCelula, dbo.relatorios_celulas.enderecoCelula, 
                      dbo.relatorios_celulas.horarioComeca, dbo.relatorios_celulas.horarioTermino, dbo.relatorios_celulas.Mensagem, dbo.relatorios_celulas.observacoes, 
                      dbo.relatorios_celulas.DisFixos, dbo.relatorios_celulas.DisPresentes, dbo.relatorios_celulas.Visitantes, dbo.relatorios_celulas.Decisoes, 
                      dbo.relatorios_celulas.valorOferta, dbo.relatorios_celulas.dataCelula, dbo.Lideres.codlider, dbo.Lideres.Nome_Lider, dbo.relatorios_celulas.dataRegistro
FROM         dbo.celulas INNER JOIN
                      dbo.Lideres ON dbo.celulas.codlider = dbo.Lideres.codlider INNER JOIN
                      dbo.relatorios_celulas ON dbo.celulas.codcelula = dbo.relatorios_celulas.codcelula
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[63] 4[10] 2[19] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "celulas"
            Begin Extent = 
               Top = 35
               Left = 74
               Bottom = 154
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Lideres"
            Begin Extent = 
               Top = 195
               Left = 331
               Bottom = 314
               Right = 521
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "relatorios_celulas"
            Begin Extent = 
               Top = 71
               Left = 660
               Bottom = 339
               Right = 850
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1920
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_Inserir_Rel_Celula'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_Inserir_Rel_Celula'
GO
/****** Object:  View [dbo].[Visao_codDiscipulo_IgualEm_LidereseDisc]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Visao_codDiscipulo_IgualEm_LidereseDisc]
AS
SELECT     dbo.discipulos.coddiscipulo, dbo.Lideres.codlider, dbo.discipulos.nome_discipulo, dbo.discipulos.E_Lider
FROM         dbo.discipulos INNER JOIN
                      dbo.Lideres ON dbo.discipulos.codlider = dbo.Lideres.codlider
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "discipulos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 205
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Lideres"
            Begin Extent = 
               Top = 55
               Left = 390
               Bottom = 175
               Right = 550
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_codDiscipulo_IgualEm_LidereseDisc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_codDiscipulo_IgualEm_LidereseDisc'
GO
/****** Object:  Table [dbo].[apostulos]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[apostulos](
	[codapos] [int] IDENTITY(1,1) NOT NULL,
	[nome_apos] [varchar](50) NULL,
	[ender_apos] [varchar](30) NULL,
	[tel_apos] [varchar](15) NULL,
	[cidade_apos] [varchar](10) NULL,
	[estado_apos] [varchar](10) NULL,
	[bairro_apos] [varchar](20) NULL,
	[sexo] [char](1) NULL,
	[codbase] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codapos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[Visao_Controle_Lideres]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Visao_Controle_Lideres]
AS
SELECT     dbo.discipulos.coddiscipulo, dbo.discipulos.nome_discipulo, dbo.discipulos.data_aceitacao, dbo.discipulos.estado_civil_discipulo, dbo.discipulos.Trabalho, dbo.discipulos.motivo_oracao, 
                      dbo.discipulos.melhor_horario, dbo.discipulos.ender_discipulo, dbo.discipulos.bairro_discipulo, dbo.discipulos.cidade_discipulo, dbo.discipulos.estado_discipulo, dbo.discipulos.nasc_discipulo, 
                      dbo.discipulos.fone_discipulo, dbo.discipulos.fone2_discipulo, dbo.discipulos.sexo, dbo.discipulos.modulo_discipulo, dbo.discipulos.situacao_discipulo, dbo.discipulos.batismo_discipulo, 
                      dbo.discipulos.data_batismo, dbo.discipulos.codpastor, dbo.discipulos.tornar_lider, dbo.discipulos.codbase, dbo.discipulos.data_cad, dbo.discipulos.E_Lider, dbo.discipulos.E_Pastor, 
                      dbo.discipulos.E_CoLider, dbo.discipulos.codlider, dbo.discipulos.datacad, dbo.discipulos.redeDiscipulo, dbo.pastores.codpastor AS Expr1, dbo.pastores.nome_pastor, 
                      dbo.Bases.codbase AS Expr2, dbo.Bases.nome_base, dbo.discipulos.E_Lider_Geracao, dbo.discipulos.codgeracao
FROM         dbo.discipulos INNER JOIN
                      dbo.Bases ON dbo.discipulos.codbase = dbo.Bases.codbase INNER JOIN
                      dbo.pastores ON dbo.discipulos.codpastor = dbo.pastores.codpastor
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[47] 4[8] 2[38] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "discipulos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 223
               Right = 316
            End
            DisplayFlags = 280
            TopColumn = 22
         End
         Begin Table = "Bases"
            Begin Extent = 
               Top = 6
               Left = 354
               Bottom = 125
               Right = 544
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pastores"
            Begin Extent = 
               Top = 165
               Left = 593
               Bottom = 284
               Right = 783
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3225
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_Controle_Lideres'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_Controle_Lideres'
GO
/****** Object:  View [dbo].[Visao_PesquisaRapida_Lideres]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Visao_PesquisaRapida_Lideres] as
select Lideres.Nome_Lider,Lideres.codlider,Lideres.coddiscipulo
from Lideres
GO
/****** Object:  View [dbo].[Visao_LiderNovo_pMultiplicacao]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Visao_LiderNovo_pMultiplicacao] as
select Lideres.nome_Lider,Lideres.codlider,Lideres.coddiscipulo
from Lideres
GO
/****** Object:  View [dbo].[Visao_LiderAtual_pMultiplicacao]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Visao_LiderAtual_pMultiplicacao] as
select Lideres.nome_Lider,Lideres.codlider,Lideres.coddiscipulo
from Lideres
GO
/****** Object:  View [dbo].[Visao_PesquisaRapida_Pastores]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Visao_PesquisaRapida_Pastores] as
select pastores.nome_pastor,pastores.codpastor,pastores.coddiscipulo
from pastores
GO
/****** Object:  View [dbo].[Visao_Geracao]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Visao_Geracao]
AS
SELECT     dbo.geracoes.codgeracao, dbo.geracoes.nomeGeracao, dbo.rede.nomeRede, dbo.Lideres.codlider, dbo.Lideres.Nome_Lider, dbo.rede.codrede, 
                      dbo.pastores.codpastor, dbo.pastores.nome_pastor, dbo.discipulos.coddiscipulo, dbo.geracoes.codlider2, dbo.geracoes.coddiscipulo2, 
                      dbo.geracoes.nomeLider2
FROM         dbo.discipulos INNER JOIN
                      dbo.Lideres ON dbo.discipulos.codlider = dbo.Lideres.codlider INNER JOIN
                      dbo.geracoes ON dbo.Lideres.codlider = dbo.geracoes.codlider INNER JOIN
                      dbo.pastores ON dbo.discipulos.codpastor = dbo.pastores.codpastor AND dbo.geracoes.codpastor = dbo.pastores.codpastor INNER JOIN
                      dbo.rede ON dbo.geracoes.codrede = dbo.rede.codrede
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[7] 4[42] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "discipulos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Lideres"
            Begin Extent = 
               Top = 32
               Left = 611
               Bottom = 151
               Right = 801
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "geracoes"
            Begin Extent = 
               Top = 201
               Left = 318
               Bottom = 320
               Right = 508
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "pastores"
            Begin Extent = 
               Top = 243
               Left = 36
               Bottom = 362
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rede"
            Begin Extent = 
               Top = 288
               Left = 617
               Bottom = 407
               Right = 807
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
    ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_Geracao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_Geracao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_Geracao'
GO
/****** Object:  View [dbo].[Visao_DiscipuloeLiderPastorRelatorio]    Script Date: 04/15/2015 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Visao_DiscipuloeLiderPastorRelatorio]
AS
SELECT     dbo.discipulos.nome_discipulo, dbo.discipulos.data_aceitacao, dbo.discipulos.estado_civil_discipulo, dbo.discipulos.Trabalho, dbo.discipulos.motivo_oracao, dbo.discipulos.melhor_horario, 
                      dbo.discipulos.ender_discipulo, dbo.discipulos.bairro_discipulo, dbo.discipulos.cidade_discipulo, dbo.discipulos.estado_discipulo, dbo.discipulos.nasc_discipulo, dbo.discipulos.fone_discipulo, 
                      dbo.discipulos.fone2_discipulo, dbo.discipulos.sexo, dbo.discipulos.modulo_discipulo, dbo.discipulos.situacao_discipulo, dbo.discipulos.batismo_discipulo, dbo.discipulos.data_batismo, 
                      dbo.discipulos.tornar_lider, dbo.discipulos.data_cad, dbo.discipulos.E_Lider, dbo.discipulos.E_Pastor, dbo.discipulos.E_CoLider, dbo.discipulos.datacad, dbo.Lideres.Nome_Lider, 
                      dbo.pastores.nome_pastor, dbo.Bases.nome_base, dbo.discipulos.redeDiscipulo, dbo.discipulos.E_Lider_Geracao, dbo.geracoes.nomeGeracao, dbo.geracoes.codgeracao
FROM         dbo.discipulos INNER JOIN
                      dbo.Lideres ON dbo.discipulos.codlider = dbo.Lideres.codlider INNER JOIN
                      dbo.pastores ON dbo.discipulos.codpastor = dbo.pastores.codpastor INNER JOIN
                      dbo.Bases ON dbo.discipulos.codbase = dbo.Bases.codbase INNER JOIN
                      dbo.geracoes ON dbo.discipulos.codgeracao = dbo.geracoes.codgeracao
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[58] 4[2] 2[26] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "discipulos"
            Begin Extent = 
               Top = 6
               Left = 9
               Bottom = 227
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "Lideres"
            Begin Extent = 
               Top = 172
               Left = 549
               Bottom = 291
               Right = 709
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pastores"
            Begin Extent = 
               Top = 8
               Left = 622
               Bottom = 130
               Right = 782
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Bases"
            Begin Extent = 
               Top = 0
               Left = 269
               Bottom = 119
               Right = 448
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "geracoes"
            Begin Extent = 
               Top = 211
               Left = 220
               Bottom = 330
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4140
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 135' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_DiscipuloeLiderPastorRelatorio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_DiscipuloeLiderPastorRelatorio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Visao_DiscipuloeLiderPastorRelatorio'
GO
/****** Object:  Default [DF_discipulos_E_Lider]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos] ADD  CONSTRAINT [DF_discipulos_E_Lider]  DEFAULT ((0)) FOR [E_Lider]
GO
/****** Object:  Default [DF_discipulos_E_Pastor]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos] ADD  CONSTRAINT [DF_discipulos_E_Pastor]  DEFAULT ((0)) FOR [E_Pastor]
GO
/****** Object:  Default [DF_discipulos_E_CoLider]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos] ADD  CONSTRAINT [DF_discipulos_E_CoLider]  DEFAULT ((0)) FOR [E_CoLider]
GO
/****** Object:  Default [DF_discipulos_E_Lider_Cobertura]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos] ADD  CONSTRAINT [DF_discipulos_E_Lider_Cobertura]  DEFAULT ((0)) FOR [E_Lider_Geracao]
GO
/****** Object:  Default [DF__Usuarios__CadDis__3C69FB99]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [CadDiscipulo]
GO
/****** Object:  Default [DF__Usuarios__CadTer__3D5E1FD2]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [CadTerritorio]
GO
/****** Object:  Default [DF__Usuarios__CadApo__3E52440B]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [CadApostulo]
GO
/****** Object:  Default [DF__Usuarios__Cad12P__3F466844]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [Cad12PrimeiraG]
GO
/****** Object:  Default [DF__Usuarios__CadRed__403A8C7D]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [CadRede]
GO
/****** Object:  Default [DF__Usuarios__CadCol__412EB0B6]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [CadColider]
GO
/****** Object:  Default [DF__Usuarios__CadLid__4222D4EF]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [CadLider]
GO
/****** Object:  Default [DF__Usuarios__CadGer__4316F928]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [CadGeracao]
GO
/****** Object:  Default [DF__Usuarios__Multip__440B1D61]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [MultiplicarCelula]
GO
/****** Object:  Default [DF__Usuarios__Alocar__44FF419A]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [AlocarDiscipulo]
GO
/****** Object:  Default [DF__Usuarios__Enviar__45F365D3]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [EnviarRelCelula]
GO
/****** Object:  Default [DF__Usuarios__Consul__46E78A0C]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [ConsultaDiscipulo]
GO
/****** Object:  Default [DF__Usuarios__Contro__47DBAE45]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [ControleLideres]
GO
/****** Object:  Default [DF__Usuarios__Relato__48CFD27E]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  DEFAULT ((0)) FOR [RelatorioGerCelula]
GO
/****** Object:  Default [DF_Usuarios_caduser]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[Usuarios] ADD  CONSTRAINT [DF_Usuarios_caduser]  DEFAULT ((0)) FOR [caduser]
GO
/****** Object:  ForeignKey [FK__apostulos__codba__7F60ED59]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[apostulos]  WITH CHECK ADD  CONSTRAINT [FK__apostulos__codba__7F60ED59] FOREIGN KEY([codbase])
REFERENCES [dbo].[Bases] ([codbase])
GO
ALTER TABLE [dbo].[apostulos] CHECK CONSTRAINT [FK__apostulos__codba__7F60ED59]
GO
/****** Object:  ForeignKey [FK__celulas__coddisc__300424B4]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[celulas]  WITH CHECK ADD FOREIGN KEY([coddiscipulo])
REFERENCES [dbo].[discipulos] ([coddiscipulo])
GO
/****** Object:  ForeignKey [FK__celulas__codgera__30F848ED]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[celulas]  WITH CHECK ADD FOREIGN KEY([codgeracao])
REFERENCES [dbo].[geracoes] ([codgeracao])
GO
/****** Object:  ForeignKey [FK__celulas__codlide__31EC6D26]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[celulas]  WITH CHECK ADD FOREIGN KEY([codlider])
REFERENCES [dbo].[Lideres] ([codlider])
GO
/****** Object:  ForeignKey [FK__celulas__codpast__32E0915F]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[celulas]  WITH CHECK ADD FOREIGN KEY([codpastor])
REFERENCES [dbo].[pastores] ([codpastor])
GO
/****** Object:  ForeignKey [FK__celulas__codrede__33D4B598]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[celulas]  WITH CHECK ADD FOREIGN KEY([codrede])
REFERENCES [dbo].[rede] ([codrede])
GO
/****** Object:  ForeignKey [FK__discipulo__codba__060DEAE8]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos]  WITH CHECK ADD  CONSTRAINT [FK__discipulo__codba__060DEAE8] FOREIGN KEY([codbase])
REFERENCES [dbo].[Bases] ([codbase])
GO
ALTER TABLE [dbo].[discipulos] CHECK CONSTRAINT [FK__discipulo__codba__060DEAE8]
GO
/****** Object:  ForeignKey [FK__discipulo__codce__37A5467C]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos]  WITH CHECK ADD FOREIGN KEY([codcelula])
REFERENCES [dbo].[celulas] ([codcelula])
GO
/****** Object:  ForeignKey [FK__discipulo__codge__2C3393D0]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos]  WITH CHECK ADD FOREIGN KEY([codgeracao])
REFERENCES [dbo].[geracoes] ([codgeracao])
GO
/****** Object:  ForeignKey [FK__discipulo__codli__1273C1CD]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos]  WITH CHECK ADD  CONSTRAINT [FK__discipulo__codli__1273C1CD] FOREIGN KEY([codlider])
REFERENCES [dbo].[Lideres] ([codlider])
GO
ALTER TABLE [dbo].[discipulos] CHECK CONSTRAINT [FK__discipulo__codli__1273C1CD]
GO
/****** Object:  ForeignKey [FK__discipulo__codpa__0519C6AF]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos]  WITH CHECK ADD  CONSTRAINT [FK__discipulo__codpa__0519C6AF] FOREIGN KEY([codpastor])
REFERENCES [dbo].[pastores] ([codpastor])
GO
ALTER TABLE [dbo].[discipulos] CHECK CONSTRAINT [FK__discipulo__codpa__0519C6AF]
GO
/****** Object:  ForeignKey [FK__discipulo__codre__2F10007B]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[discipulos]  WITH CHECK ADD FOREIGN KEY([codrede])
REFERENCES [dbo].[rede] ([codrede])
GO
/****** Object:  ForeignKey [FK__geracoes__codlid__286302EC]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[geracoes]  WITH CHECK ADD FOREIGN KEY([codlider])
REFERENCES [dbo].[Lideres] ([codlider])
GO
/****** Object:  ForeignKey [FK__geracoes__codpas__29572725]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[geracoes]  WITH CHECK ADD FOREIGN KEY([codpastor])
REFERENCES [dbo].[pastores] ([codpastor])
GO
/****** Object:  ForeignKey [FK__geracoes__codred__2A4B4B5E]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[geracoes]  WITH CHECK ADD FOREIGN KEY([codrede])
REFERENCES [dbo].[rede] ([codrede])
GO
/****** Object:  ForeignKey [FK__pastores__codbas__24927208]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[pastores]  WITH CHECK ADD  CONSTRAINT [FK__pastores__codbas__24927208] FOREIGN KEY([codbase])
REFERENCES [dbo].[Bases] ([codbase])
GO
ALTER TABLE [dbo].[pastores] CHECK CONSTRAINT [FK__pastores__codbas__24927208]
GO
/****** Object:  ForeignKey [FK__relatorio__codce__267ABA7A]    Script Date: 04/15/2015 11:22:27 ******/
ALTER TABLE [dbo].[relatorios_celulas]  WITH CHECK ADD FOREIGN KEY([codcelula])
REFERENCES [dbo].[celulas] ([codcelula])
GO
